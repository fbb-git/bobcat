<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::OFilterBuf(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::OFilterBuf(3bobcat)</h1>
<h2 id="author">ostream filtering<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::OFilterBuf - Base class for std::ostream filtering
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/ofilterbuf&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
<p>
The <strong >FBB::OFilterBuf</strong> class is a specialization of the
<em >std::streambuf</em> class and can be used as a base class for classes
implementing <em >ostream</em>-filtering.
<p>
Ostream filtering is defined here as the process by which inserted characters
are subject to processing before they are passed on to another (filtered)
<em >ostream</em> object (or they may be rejected). The filtering may also result in
inserting additional information into the filtered <em >ostream</em>.
<p>
<em >Chaining</em> of filters is also possible: the filtered <em >ostream</em> may itself
use an <em >OFilterBuf</em> to filter its received information before passing
it on to yet another <em >ostream</em>.
<p>
As <em >OFilterBuf</em> inherits from <em >std::streambuf</em> an
<em >OFilterBuf</em> object can be used to provide an <em >ostream</em> object
with a <em >std::streambuf</em>. Information inserted into such a stream travels the
following route:
    <ul>
    <li> The information is converted to characters using the standard
conversion facilities implemented by <em >std::ostream</em> objects. E.g., when
inserting the value <em >123</em> this value is converted to
the characters <em >'1', '2'</em> and <em >'3'</em>, respectively.
    <li> Each of the characters is then offered (in turn) to the
<em >std::streambuf</em> that is associated with the <em >ostream</em> object. In
particular, the <em >std::streambuf</em>'s <em >overflow()</em> member is called.
    <li> <em >OFstreamBuf</em>'s default <em >overflow()</em> function ignores characters,
but specializations can override <em >overflow()</em> to process the received
characters <em >ad lib</em>.
    <li> A overriding <em >overflow()</em> function has access to the member
<em >OFstreambuf::out()</em> which is a reference to the <em >std::ostream</em> receiving
the filtered information.
    </ul>
    To implement a simple copy-filter (i.e., all characters are accepted
as-is) a class must be derived from <em >OFilterBuf</em> providing an
overriding implementation of <em >overflow()</em>, e.g., as follows:
        <pre>

    int DerivedClass::overflow(int ch)
    {
        out().put(ch);
    }
        
</pre>

    Next this <em >std::streambuf</em> specialization can be associated with an
<em >ostream</em> into which information to be `copy filtered' can be inserted
(cf. the EXAMPLE section below).
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    std::streambuf
<p>
<h2 >CONSTRUCTORS</h2>
    As <em >OFilterBuf</em> should be used as a base class all its
constructors are protected.
<p>
<ul>
    <li> <strong >OFilterBuf()</strong>:<br/>
        This constructor creates a <em >OFilterBuf</em> object without
associating it with a destination (filtered) <em >ostream</em>.
<p>
<li> <strong >OFilterBuf(std::string const &amp;fname,
                                            openmode mode = std::ios::out)</strong>:<br/>
        This constructor creates a <em >OFilterBuf</em> object and opens a
private <em >std::ofstream</em> object whose filename is provided and that should
receive the filtered information.
<p>
<li> <strong >OFilterBuf(std::ostream &amp;out)</strong>:<br/>
        This constructor creates a <em >OFilterBuf</em> object and will insert
any filtered information into the provided  <em >ostream</em> object.
    </ul>
<p>
Copy and move constructors (and assignment operators) are not available.
<p>
<h2 >PROTECTED MEMBER FUNCTIONS</h2>
<p>
Except for the public members inherited from <strong >std::ostreambuf</strong> all of
<em >OFilterBuf's</em> members are protected.
<p>
Derived classes should provide their own implementation of <em >int
underflow(int ch)</em> to implement filtering.
<p>
<ul>
    <li> <strong >void reset(std::string const &amp;fname, openmode mode = std::ios::out)</strong>:<br/>
        This member flushes the current destination (filtered)
<em >std::ostream</em> and associates the <em >OFilterBuf</em> object with an
<em >std::ofstream</em> object whose filename is provided and that should receive
subsequently filtered information.
<p>
<li> <strong >void reset(std::ostream &amp;out)</strong>:<br/>
        This member flushes the current destination (filtered)
<em >std::ostream</em> object and associates the <em >OFilterBuf</em> object with
the provided <em >ostream</em> object.
<p>
<li> <strong >std::ostream &amp;out() const</strong>:<br/>
        This member is available to derived classes to insert information into
the destination (filtered) stream.
    </ul>
<p>
<h2 >EXAMPLE</h2>
        <pre>

    #include &lt;iostream&gt;
    #include &lt;cctype&gt;
    #include &lt;bobcat/ofilterbuf&gt;

    struct NoDigits: public FBB::OFilterBuf
    {
        NoDigits(std::ostream &amp;out)
        :
            OFilterBuf(out)
        {}

        private:
            int overflow(int ch) override
            {
                if (not isdigit(ch))
                    out().put(ch);
                return ch;
            }
    };

    using namespace FBB;
    using namespace std;

    int main()
    {
        NoDigits nod(cout);     // no digits to cout
        ostream out(&amp;nod);

        out &lt;&lt; cin.rdbuf();      // rm digits from cin
    }
        
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/ofilterbuf</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >ifilterbuf</strong>(3bobcat)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
