<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::ArgConfig(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::ArgConfig(3bobcat)</h1>
<h2 id="author">Program Arguments<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::ArgConfig - A singleton class processing program arguments
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/argconfig&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
    Singleton class (see Gamma <em >et al.</em>, 1995) built around
<strong >getopt_long()</strong>(3). The class handles short- and long command-line options,
as well as configuration files.
<p>
In addition to the standard command-line options the various <em >option</em>
members also recognize long options as keys, optionally followed by a colon
and an option value are also recognized. E.g., an option <em >--input filename</em>
can be specified in the configuration file like
        <pre>

    input: filename
        
</pre>

or
        <pre>

    input filename
        
</pre>

    Options without arguments should probably not use the colon, although it
is accepted by <em >ConfigArg</em>. E.g., for the option <em >--verbose</em> both forms
are accepted:
    <pre>

verbose
verbose:
    
</pre>

<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    <strong >FBB::Arg</strong>,<br/>
    <strong >FBB::ConfigFile</strong>
<p>
<h2 >ENUMERATION</h2>
<p>
The <strong >FBB::ArgConfig::Type</strong> enumeration is inherited from the
<strong >FBB::Arg</strong> class. It is used to specify whether or not long options require
arguments. It defines the following values: <strong >None, Required, Optional</strong>.
    <ul>
    <li> <strong >None</strong>: the long option does not use an argument;
    <li> <strong >Required</strong>: the long option requires an argument value;
    <li> <strong >Optional</strong>: the long option may optionally be provided with  an
        argument value;
    </ul>
<p>
These values are used when defining long options (like <em >--version</em>), which
are defined as objects of the (nested inherited) class
<strong >FBB::Arg::LongOption</strong> (in the context of <em >ArgConfig</em> this is identical to
<strong >FBB::ArgConfig::LongOption</strong>.
<p>
<h2 >THE NESTED INHERITED CLASS FBB::Arg::LongOption</h2>
   Long options are defined using objects of the nested class
    <strong >FBB::Arg::LongOption</strong>. This class provides the following constructors:
   <ul>
    <li> <strong >FBB::Arg::LongOption(char const *name, FBB::Arg::Type type =
        FBB::Arg::None)</strong>:<br/>
       This constructor is used to define a long option for which no
        corresponding short option is defined. The parameter <em >name</em> is the
        name of the long option (without specifying the -- characters which
        are only required when specifying a long option when calling a
        program).
<p>
<li> <strong >FBB::Arg::LongOption(char const *name, int optionChar)</strong>:<br/>
       This constructor is used to define a long option for which a
        corresponding short option is defined. The parameter <em >name</em> is the
        name of the long option (without specifying the -- characters which
        are only required when specifying a long option when calling a
        program).
    </ul>
<p>
To define long options use the following procedure:
    <ul>
    <li> First, construct an array
        <pre>

    FBB::Arg::LongOption longOptions[] = { c1, c2, ..., cn };
        
</pre>

    Where <em >c1, c2, ..., cn</em> are <em >n</em> constructor invocations of
        <strong >FBB::Arg::LongOption()</strong> constructors
<p>
<li> Next, pass <em >longOptions, LongOptions + n</em> as arguments to an
        <em >Arg::initialize</em> member that supports long options.
    </ul>
<p>
Objects of the class <em >LongOptions</em> are normally used internally by the
<em >ArgConfig</em> object, but they can also be used outside of the <em >ArgConfig</em>
object. For that situation the following members are available:
    <ul>
    <li> <strong >std::string const &amp;longName() const</strong>:<br/>
       returns the <em >LongOption's</em> long option name;
    <li> <strong >int optionChar() const</strong>:<br/>
       returns the <em >LongOption's</em> option character (or one of the
        <em >Arg::Type</em> enumeration values if there is no option character
        associated with the <em >LongOption</em>).
    </ul>
<p>
<h2 >CONSTRUCTORS</h2>
    Since the class is a <em >Singleton</em>, no public constructors are
available. Instead, static members are offered to initialize and access the
single <strong >ArgConfig</strong> object.
<p>
<h2 >STATIC MEMBERS</h2>
<p>
All <em >initialize</em> members initialize the <strong >FBB::ArgConfig</strong> singleton,
and can only be called once. An exception is thrown when called multiple
times. All <em >initialize</em> members return a reference to the initialized
<em >ArgConfig</em> singleton object.
<p>
All <em >initialize</em> members define the parameters <em >argc</em> and <em >argv</em>
which are interpreted as <em >main's</em> <em >argc</em> and <em >argv</em> parameters.  When an
<em >argv</em> element points to two consecutive dashes (<em >--</em>) then that element
is ignored, and all of <em >argv's</em> subsequent elements are considered arguments
instead of options.
<p>
<ul>
    <li> <strong >FBB::ArgConfig &amp;ArgConfig::initialize(char const *optstring, int argc,
        char **argv, [std::string const &amp;fname,] Comment cType = KeepComment,
        SearchCasing sType = SearchCaseSensitive, Indices iType =
        IgnoreIndices)</strong>:<br/>
       The parameter <em >optstring</em> is a null-terminated byte string (NTBS)
        optionally starting with a + character, but otherwise containing
        option characters. One or two colons may be postfixed to option
        characters:
       <blockquote >
       <ul>
        <li> a single colon (:) indicates that the option requires an
            option value.
        <li> a double colon (::) indicates that the option has an optional
            argument. With short options the option value is considered absent
            unless it is attached to the short option (e.g.,
            <em >-tvalue</em>). Long options optionally accepting arguments should
            always immediately be followed by an assignment character (=),
            immediately followed by the option's value (which must start with
            a non-blank character). E.g., <em >--value=</em> indicates an absent
            option value, <em >--value=text</em> indicates the option's value equals
            <em >text</em>.  If an option value itself contains blanks, it must be
            surrounded by single or double quotes (e.g., <em >-t'this value'</em>,
            or <em >--text='this value'</em>). The surrounding quotes are not part
            of the option's value.
        </ul></blockquote>
<p>
When <em >optstring's</em> first character is + then all non-specified
        options are considered arguments, appearing in the final arguments
        list at their current argument positions. E.g., when <em >optstring</em>
        is <em >+ab</em> and no long options are defined, then calling
    <pre>

    prog -a -z -b -yvalue --long arg1 arg2
    
</pre>

       results in the member <em >argv</em> returning a vector containing the
        elements <em >-z, -yvalue, --long, arg1,</em> and <em >arg2</em>. If
        <em >optstring's</em> first character isn't + and an undefined option is
        encountered then an exception is thrown.
<p>
The <em >fname</em> argument is optional. If provided, a configuration file
        by the specified name is opened (and must exist); if omitted the
        <em >ArgConfig</em> is created without using a configuration file. In the
        latter case a configuration file may be specified later using the
        <em >open</em> member inherited from <em >ConfigFile</em>.
<p>
The final three parameters are <em >ConfigFile</em> parameters, receiving the
        shown default values. This constructor returns a reference to the
        singleton object, allowing code initializing <strong >ArgConfig</strong> to use the
        initialized object immediately.
<p>
<li> <strong >FBB::ArgConfig &amp;ArgConfig::initialize(int accept. char const
        *optstring, int argc, char **argv, [std::string const &amp;fname,] Comment
        cType = KeepComment, SearchCasing sType = SearchCaseSensitive, Indices
        iType = IgnoreIndices)</strong>:<br/>
       Acts like the previous member, but in addition defines the
        parameter <em >accept</em> specifying an option character from where all
        subsequent arguments and options are considered arguments. To ignore
        <em >accept</em> the value 0 (not the character '0') can be specified or an
        <em >initialize</em> members can be used that does not define an
        <em >accept</em> parameter.
<p>
When arguments contain both an <em >accept</em> option and two consecutive
        dashes then the first one is interpreted, resulting in all remaining
        <em >argv</em> elements being interpreted as mere arguments. For example,
        when specifying <em >initialize('t', ...)</em> and calling
    <pre>

    prog one -ttwo -c -- three
    
</pre>

       then the member <em >argv</em> returns a vector containing the elements
        <em >one, -tttwo, -c, --</em>, and <em >three</em> (see also the member
        <em >beyondDashes</em> below).
<p>
<li> <strong >FBB::ArgConfig &amp;ArgConfig::initialize(char const *optstring,
        ArgConfig::LongOption const *const begin, ArgConfig::LongOption const
        *const end, int argc, char **argv, [std::string const &amp;fname,] Comment
        cType = KeepComment, SearchCasing sType = SearchCaseSensitive, Indices
        iType = IgnoreIndices)</strong>:<br/>
      Acts like the first <em >ArgConfig::initialize</em> member, but in addition
        defines two parameters specifying the range of elements of an array of
        <em >ArgConfig::LongOption</em> objects specifying long options. The
        parameter <em >begin</em> points to the first element of the range, the
        parameter <em >end</em> points just beyond the last element of the
        range. E.g., after defining
    <pre>

    FBB::ArgConfig::LongOption longOptions[] = { c1, c2, ..., cn };
    
</pre>

       the arguments passed to <em >begin</em> and <em >end</em> could be specified as
    <pre>

    initialize(..., longOptions, longOptions + size(longOptions), ...);
    
</pre>

<p>
<li> <strong >FBB::ArgConfig &amp;ArgConfig::initialize(char accept, char const
        *optstring, LongOption const *const begin, LongOption const *const
        end, int argc, char **argv, [std::string const &amp;fname,] Comment cType
        = KeepComment, SearchCasing sType = SearchCaseSensitive, Indices iType
        = IgnoreIndices)</strong>:<br/>
<p>
Acts like the previous <em >ArgConfig::initialize</em> member, but in addition
        defines an <em >accept</em> parameter as defined by the second
        <em >ArgConfig::initialize</em> member.
<p>
<li> <strong >FBB::ArgConfig &amp;ArgConfig::instance()</strong>:<br/>
       Once an <em >ArgConfig::initialize</em> member has been called this member
        can be called from anywhere in the program (and it can be called
        multiple times), returning a reference to the initialized
        <strong >ArgConfig</strong> object.
<p>
If it is called before an <em >ArgConfig::initialize</em> member has been
        called an exception is thrown.
    </ul>
<p>
<li> <strong >FBB::ArgConfig &amp;instance()</strong>:<br/>
       Returns the instance of the <strong >ArgConfig</strong> object, available after
        calling one of the <strong >ArgConfig::initialize</strong> members.  If called
        before initialization, an <em >FBB::Exception</em> exception is thrown.
    )
<p>
<h2 >NON-STATIC MEMBER FUNCTIONS</h2>
    All public members of the <em >Arg</em> and <em >ConfigFile</em> classes are also
offered by the <em >ArgConfig</em> class. As several  <em >Arg::option</em> members were
reimplemented by <em >ArgConfig</em> all <em >option</em> members are discussed below. All
other members inherit straight from the classes <em >Arg</em> and
<em >ConfigFile</em>. Consult their man pages for details.
    <ul>
    <li> <strong >size_t option(int option) const</strong>:<br/>
       Returns the number of times `option' (or its long option synonym, if
        defined) was specified as command line option or as as a configuration
        file option.
    <li> <strong >size_t option(std::string const &amp;options) const</strong>:<br/>
       Returns the total number of times any of the characters specified in
        the `options' string (or their long option synonyms) was specified as
        command line option or as as a configuration file option.
    <li> <strong >size_t option(string *value, int option) const</strong>:<br/>
       Returns the number of times the provided option (or its long option
        synonym) was present as either a command line option or as a
        configuration file option. If the return value is non-zero then the
        value of the first occurrence of this option (first checking the
        command line options; then checking the configuration file) is stored
        in <em >*value</em>, which is left untouched if `option' was not present. 0
        may be specified for <strong >value</strong> if the option does not have a value or
        if the value should not be stored.
    <li> <strong >size_t option(size_t idx, string *value, int option) const</strong>:<br/>
       This member acts identically to the <em >Arg::option</em> member having the
        identical prototype. It does not consider the configuration file but
        merely returns the number of times the provided option (or its long
        option synonym) was present. If the return value is non-zero then the
        value of the <em >idx</em>th occurrence (0-based offset) of this option is
        stored in <em >*value</em>, which is left untouched if `option' was not
        present or if <em >idx</em> is or exceeds the number of specifications of
        the provided option. 0 may be specified for <strong >value</strong> if the option
        does not have a value or if the value should not be stored.
    <li> <strong >size_t option(size_t *idx, string *value, int option) const</strong>:<br/>
       This member acts identically to the <em >Arg::option</em> member having the
        identical prototype. It does not consider the configuration file but
        merely returns the number of times the provided option (or its long
        option synonym) was present. If the return value is non-zero then the
        offset (within the series of <em >option</em> specifications) of the first
        option having a non-empty option value is returned in <em >*idx</em>, while
        its option value is stored in <em >*value</em>. Both <em >*value</em> and <em >*idx</em>
        are left untouched if `option' was not present. 0 may be specified for
        <strong >value</strong> if the option does not have a value or if the value should
        not be stored.
    <li> <strong >size_t option(string *value, char const *longOption) const</strong>:<br/>
       Returns the number of times the specified long option (not having a
        single-character synonym) was present as either a command line option
        or in the configuration file. If found, then the value found at the
        first occurrence of the option (first considering the command line
        options, then the configuration file) is stored in <em >*value</em>. The
        string pointed to by <em >value</em> is left untouched if the long option
        was not present. 0 may be specified for <strong >value</strong> if the option does
        not have a value or if the value should not be stored.
    <li> <strong >size_t option(size_t idx, string *value,
                                        char const * longOption) const</strong>:<br/>
       This member acts identically to the <em >Arg::option</em> member having the
        identical prototype. It does not consider the configuration file but
        merely returns the number of times the provided long option (not
        having a single-character synonym) was present. If the return value is
        non-zero then the value of the <em >idx</em>th occurrence (0-based offset)
        of this long option is stored in <em >*value</em>, which is left untouched
        if the long option was not present or if <em >idx</em> is or exceeds the
        number of specifications of the provided long option. 0 may be
        specified for <strong >value</strong> if the long option does not have a value or if
        the value should not be stored.
    <li> <strong >size_t option(size_t *idx, string *value, int longOption) const</strong>:<br/>
       This member acts identically to the <em >Arg::option</em> member having the
        identical prototype. It does not consider the configuration file but
        merely returns the number of times the provided long option (not
        having a single-character synonym) was present. If the return value is
        non-zero then the offset (within the series of this long option
        specifications) of the first long option having a non-empty option
        value is returned in <em >*idx</em>, while its option value is stored in
        <em >*value</em>. Both <em >*value</em> and <em >*idx</em> are left untouched if long
        option was not present. 0 may be specified for <strong >value</strong> if the long
        option does not have a value or if the value should not be stored.
    </ul>
<p>
<h2 >EXAMPLE</h2>
<p>
<pre>

#include &lt;iostream&gt;
#include &lt;string&gt;

#include &lt;bobcat/argconfig&gt;
#include &lt;bobcat/exception&gt;

using namespace std;
using namespace FBB;

ArgConfig::LongOption lo[] =
{
    ArgConfig::LongOption{"option", 'o'},
    ArgConfig::LongOption{"value-option", 'v'}
};

class X
{
    ArgConfig &amp;d_arg;

    public:
        X();
        void function();
};

X::X()
:
    d_arg(ArgConfig::instance())
{}

void X::function()
{
    if (d_arg.nArgs() == 0)
        throw Exception() &lt;&lt; "Provide the name of a config file as 1st arg";

    cout &lt;&lt; "Counting " &lt;&lt; d_arg.option('o') &lt;&lt; " instances of -o or "
                                                            "--option\n";

    d_arg.open(d_arg[0]);       // Now open the config file explicitly
                            // (alternatively: use a constructor expecting
                            // a file name)

    cout &lt;&lt; "Counting " &lt;&lt; d_arg.option('o') &lt;&lt; " instances of -o or "
                                                            "--option\n";

    string optval;
    size_t count = d_arg.option(&amp;optval, 'v');

    cout &lt;&lt; "Counting " &lt;&lt; count &lt;&lt;
                        " instances of -v or --value-option\n";
    if (count)
        cout &lt;&lt; "Option value = " &lt;&lt; optval &lt;&lt; endl;
}

int main(int argc, char **argv)
try
{
    ArgConfig::initialize("ov:", lo, lo + 2, argc, argv);

    X x;
    x.function();
}
catch (Exception const &amp;err)
{
    cout &lt;&lt; "Terminating " &lt;&lt; err.what() &lt;&lt; endl;
    return 1;
}
    
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/argconfig</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >arg</strong>(3bobcat), <strong >configfile</strong>(3obcat), <strong >bobcat</strong>(7)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
