<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::IFilterBuf(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::IFilterBuf(3bobcat)</h1>
<h2 id="author">Filtering Input Stream Buffer<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::IFilterBuf - Filtering stream buffer initialized by a std::istream object
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/ifilterbuf&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
    <strong >FBB::IFilterBuf</strong> objects may be used as a <strong >std::streambuf</strong> for
<em >std::istream</em> objects, filtering the information produced by those objects.
<p>
Because <em >IFilterBuf</em> is a <em >streambuf</em> its member <em >underflow</em> is
automatically called when reading operations are requested from the <em >stream</em>
object using the <em >IFilterBuf</em> as its <em >streambuf</em>. If no chars are
currently available (i.e., <em >srcBegin == srcEnd</em>, see the description of the
<em >filter</em> member below), then <em >filter</em> is called, which may store
characters in a (local) buffer of at most <em >maxSize</em> characters (see the
description of the <em >IFilterBuf</em> constructor below). Once this buffer has
been filled <em >filter</em> updates the <em >*srcBegin</em> and <em >*srcEnd</em> pointers so
that they point to, respectively, the the location of the first character in
the local buffer and beyond the location of the last character in the local
buffer.
<p>
The class <em >IFilterBuf</em> was designed with the <strong >openSSL BIO</strong>
(cf. <strong >bio</strong>(3ssl)) in mind. Since the BIO concept was developed in the
context of the <strong >C</strong> programming language, BIOs do not support <strong >C++</strong>
streams. Nonetheless, the concept of a filtering device is an attractive one,
and is offered by the <strong >FBB::IFilterBuf</strong> class.
<p>
In addition to filtering, <strong >IFilterBuf</strong> offers flexible internal buffer
management: derived classes can put characters back on the internal buffer
until the beginning of the buffer has been reached, but may then continue
pushing characters on the buffer until the buffer has reached its maximum
size. This maximum size is defined by the constructor's <em >maxSize</em> parameter
(see below).
<p>
The class <strong >IFilterBuf</strong> is an abstract base class. It is used via
classes that are derived from <strong >IFilterBuf</strong>, implementing its pure
virtual <em >load</em> member (see below at <strong >PRIVATE VIRTUAL MEMBER FUNCTIONS</strong>).
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    <strong >std::streambuf</strong>
<p>
<h2 >MEMBER FUNCTIONS</h2>
     All members of <strong >std::streambuf</strong> are available, as <strong >IFilterBuf</strong>
inherits from this class.
<p>
<h2 >PROTECTED CONSTRUCTOR</h2>
    <ul>
    <li> <strong >IFilterBuf(size_t maxSize = 1000)</strong>:<br/>
        This constructor initializes the streambuf. While the streambuf is
being used, its internally used buffer is gradually filled. It may be filled
with up to <em >maxSize</em> characters, but the actual number of characters that is
stored in the buffer is determined by the member <em >filter</em> (see below) and by
using the member <em >streambuf::sputbackc</em>.
    </ul>
<p>
Copy and move constructors (and assignment operators) are not available.
<p>
<h2 >PROTECTED MEMBER FUNCTION</h2>
<p>
<ul>
    <li> <strong >void setBuffer()</strong>:<br/>
        This member initializes the base class's buffer pointers (i.e.,
<em >eback, gptr,</em> and <em >egptr</em>) with the initial range of characters retrieved
by <em >filter</em> (see below).
<p>
Derived classes do not have to call this member, but if they do they
should only call <em >setBuffer</em> once from their constructors. Once
<em >setBuffer</em> has been called, the <em >peek</em> member of the <em >std::istream</em>
that is available to <strong >IFilterBuf</strong> objects can be called to inspect the
next available character, even if no other stream operation has as yet been
performed. If it is not called by the derived class's constructor, then
<em >peek</em> returns 0 until at least one character has been retrieved from the
<em >istream</em> object.
    </ul>
<p>
<h2 >PRIVATE VIRTUAL MEMBER FUNCTIONS</h2>
<p>
<ul>
    <li> <strong >virtual bool filter(char const **srcBegin, char const **srcEnd) = 0</strong>:<br/>
        The <em >filter</em> member is declared as a pure virtual member: derived
classes <em >must</em> override <em >filter</em> with their own implementation.
<p>
Derived class objects are responsible for obtaining information (in any
amount) from the device with which they interact. This information is then
passed on to the <em >IFilterBuf</em> via two pointers, pointing,
respectively, to the first available character and beyond the last available
character. The characters indicated by this range are subsequently transferred
by the <strong >IFilterBuf</strong> object to its own buffer, from where they are then
retrieved (or to where they can be pushed back) by the application.
<p>
The <em >filter</em> member allows implementations to filter and/or modify the
information that is obtained by this member. The <strong >EXAMPLE</strong> section below
provides an example filtering out a configurable set of characters from a
provided <em >std::istream</em>. Bobcat's classes <strong >ISymCryptStreambuf</strong>(3bobcat)
and <strong >IBase64Buf</strong>(3bobcat) provide additional examples of classes
derived from  <strong >IFilterBuf</strong>.
<p>
The <em >filter</em> member should return <em >false</em> if no (more) information is
available. It should return <em >true</em> if information is available, in which
case <em >*srcBegin</em> and <em >*srcEnd</em> should be pointing to, respectively, the
first character and beyond the last character made available by <em >filter</em>;
<p>
<li> <strong >int pbackfail(int ch) override</strong>:<br/>
       If <em >IFilterBuf's</em> internally used buffer has reached its
        maximmum size then EOF is returned. Otherwise, <em >ch</em> is inserted at
        the beginning of the internally used buffer, becoming the next
        character that's retrieved from the object's buffer;
<p>
<li> <strong >std::streamsize showmanyc() override</strong>:<br/>
       The sum of the number of not yet processed characters in the internally
        used buffer and the number of not yet processed characters returned
        by the latest <em >filter</em> call is returned;
<p>
<li> <strong >int underflow() override</strong>:<br/>
       Once the internally used buffer is empty <em >filter</em> is called to obtain
        a new series of filtered characters. If <em >filter</em> returns <em >false
        underflow</em> returns EOF. Otherwise the series of characters returned by
        <em >filter</em> are transferred to the <em >IFilterBuf's</em> internal
        buffer to be processed by the <em >std::istream</em> that's initialized with
        the <em >IFilterBuf</em> object.
    </ul>
<p>
<h2 >EXAMPLE</h2>
<p>
Here is a class, derived from <em >IFilterBuf</em>, filtering out a
predefined set of characters. It is used twice to filter digits and
vowels, illustrating chaining of <strong >IFilterBuf</strong> objects.
<p>
<pre >
#include &lt;iostream&gt;
#include &lt;fstream&gt;
#include &lt;istream&gt;
#include &lt;string&gt;

#include &lt;bobcat/ifilterbuf&gt;

using namespace std;
using namespace FBB;

class CharFilterStreambuf: public IFilterBuf
{
    istream &amp;d_in;         // stream to read from
    string d_rmChars;      // chars to rm
    string d_buffer;       // locally buffered chars
    size_t const d_maxSize = 100;

    public:
        CharFilterStreambuf(istream &amp;in, string const &amp;rmChars);

    private:
        bool filter(char const **srcBegin,
                    char const **srcEnd) override;
};

CharFilterStreambuf::CharFilterStreambuf(istream &amp;in,
                                         string const &amp;rmChars)
:
    d_in(in),
    d_rmChars(rmChars)
{
    setBuffer();        // required if peek() must return the 1st
}                       // available character right from the start

bool CharFilterStreambuf::filter(char const **srcBegin,
                                 char const **srcEnd)
{
    d_buffer.clear();

    while (d_buffer.size() != d_maxSize)
    {
        char ch;
        if (not d_in.get(ch))
            break;
        if (d_rmChars.find(ch) != string::npos) // found char to rm
            continue;
        d_buffer.push_back(ch);
    }

    if (d_buffer.empty())
        return false;

    *srcBegin = d_buffer.data();
    *srcEnd = d_buffer.data() + d_buffer.size();

    return true;
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        cout &lt;&lt; "arg[1]: file to process, arg[2]: processed file\n";
        return 0;
    }

    ifstream in{ argv[1] };
    CharFilterStreambuf buf1(in, "1234567890");
    istream in1(&amp;buf1);

    CharFilterStreambuf buf2(in1, "AEIOUaeiou");
    istream in2(&amp;buf2);

    ofstream out{ argv[2] };
    out &lt;&lt; in2.rdbuf();
}
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/ifdbuf</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >isymcryptstreambuf</strong>(3bobcat),
<strong >ibase64buf</strong>(3bobcat),
<strong >ofilterbuf</strong>(3bobcat). <strong >std::streambuf</strong>
<p>
<h2 >BUGS</h2>
    None reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
