<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::CSVTable(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::CSVTable(3bobcat)</h1>
<h2 id="author">Table Construction<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::CSVTable - Sequentially fills tables row-wise
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/csvtable&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
<p>
<strong >FBB::CSVTable</strong> is used to fill tables row-wise. By default the table's
elements are comma-separated. The elements may contain any type of data that
can also be inserted into <em >std::ostreams</em>, as may also contain horizontal
lines (optionally spanning multiple columns).
<p>
Before inserting elements into the table the widths, alignment types and
precisions of the table's columns are defined. By default values are
right-aligned.  While inserting table elements the alignment types and
precisions may be altered for specific elements, optionally spanning multiple
columns. When inserting values whose representations require more characters
than the current widths of the columns receiving those values then those
larger widths take precedence over the defined column widths.
<p>
Different from tables defined by <strong >FBB::Table</strong>(3bobcat) all data inserted
into <em >CSVTables</em> do not have to be completely available before the table
is inserted into a destination <em >std::ostream</em>. As the table's column formats
are known before entering the data the <em >CSVTable</em> knows which format to use
for which column. These column format specifications may be defined in
multiple ways, e.g., by using text labels and values. <em >CSVTable</em> objects
always use the widest column specifications and alignment types that were
specified last.
<p>
When inserting elements into <em >CSVTables</em> the standard <strong >C++</strong> IO
manipulators can also be used. Table rows do not automatically end after the
table's last column has been filled. But when inserting elements beyond the
last column they are inserted as-is (but then the standard I/O format
specifications can still be used).
<p>
Table column definitions and table rows end at the end of insertion
statements (see below at the descriptions of the various <em >operator&lt;&lt;</em>
functions). E.g.,
        <pre>

    CSVTable tab;
    ...
    tab &lt;&lt; 1 &lt;&lt; 2;              // two elements in this row
    tab &lt;&lt; "one" &lt;&lt; "two" &lt;&lt; 3; // three elements in this row
        
</pre>

<p>
<em >CSVTable</em> uses two support classes handling, respectively, the definitions
of the characteristics of the table's columns and inserting values into the
table's elements. <em >CSVTabDef</em> handles the table's column definitions,
<em >CSVTabIns</em> handles insertions into the table elements. They offer various
insertion operators which are described below.
<p>
Constructing tables normally consists of two steps: first the characteristics
of the columns are defined, then values are inserted into the table's
elements. This sequence is not enforced by <em >CSVTable</em>: after inserting
values into the table column definitions may be updated, whereafter
additional values may be inserted into the table which then use the updated
column definitions.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    -
<p>
<h2 >FMT</h2>
<p>
<strong >FBB::FMT</strong> objects are returned by several free functions (like <em >left</em>,
described below in section <strong >FREE FUNCTIONS</strong>), and <em >FMT</em> defines the
enumeration <em >Align</em> (see the next section) specifying alignment
types. <em >FMT</em> objects are internally used by <em >CSVTable</em> objects. A <em >FMT</em>
object specifies the width, the precision when floating point numbers are
inserted, the column's alignment type (left, right or centered), and the
number of table columns to use.
<p>
<em >FMT</em> objects can be inserted into <em >std::ostream</em> objects showing its
characteristics. <em >FMT</em> provides the following (const) accessors:
    <ul>
    <li> <strong >FMT::Align align()</strong>:<br/>
        the alignment value;
    <li> <strong >unsigned nCols()</strong>:<br/>
        the number of occupied columns;
    <li> <strong >unsigned precision()</strong>:<br/>
       the precision used when inserting a floating point value (<em >~0U</em> (= -1
        as <em >int</em>) is returned if <em >precision</em> is not used). The insertion
        operator shows <em >precision: -1</em> when precision is <em >~0U</em>;
    <li> <strong >unsigned width()</strong>:<br/>
        the field width in number of characters;
    </ul>
<p>
The static member <em >char const *FMT::align(FMT::Align value)</em> returns the
textual label corresponding to <em >value</em>.
<p>
<h2 >ALIGN ENUM</h2>
<p>
The <em >enum FMT::Align</em> defines values indicating the alignment types of the
table's columns:
    <ul>
    <li> <strong >FMT::Align::CENTER</strong>:<br/>
        The inserted information in the column is centered;
    <li> <strong >FMT::Align::LEFT</strong>:<br/>
        The inserted information is left-aligned;
    <li> <strong >FMT::Align::RIGHT</strong>:<br/>
        The inserted information is right-aligned (used by default);
    </ul>
<p>
In addition, when inserting horizontal lines, the value
    <em >FMT::Align::HLINE</em> is used.
<p>
<h2 >CONSTRUCTORS</h2>
    <ul>
    <li> <strong >CSVTable(std::ostream &amp;out = std::cout,
                 std::string const &amp;sep = ", ")</strong>:<br/>
       This constructor by default writes its table to <em >std::cout</em> and uses
        a comma followed by a space character as column separator. During the
        table's construction the stream receiving the table can be altered
        using <em >stream</em> members, and the separator can be changed using the
        <em >sep</em> member, but the separator can also be changed while filling
        the table's elements (see below). When the <em >CSVTable</em> object goes
        out of scope the stream's original configuration is restored;
<p>
<li> <strong >CSVTable(std::ofstream &amp;&amp;tmp, std::string const &amp;sep = ", ")</strong>:<br/>
       This constructor by default uses the same separator to separate the
        column's elements as the first constructor, but writes the table to
        <em >ofstream tmp</em>, which is grabbed by <em >CSVTable</em>;
<p>
<li> <strong >CSVTable(std::string const &amp;fname,
                 std::string const &amp;sep = ", "),
                 std::ios::openmode mode = ios::out</strong>:<br/>
       This constructor by default uses the same separator to separate the
        column's elements as the first constructor, but writes the table to
        the file having (path)name <em >fname</em>, by default (re)writing the
        file. If the file already exists and <em >CSVTable</em> should start writing
        at the file's end use, e.g., <em >ios::ate | ios::in</em>. An exception is
        thrown if the specified file cannot be opened.
    </ul>
<p>
The move constructor and move assignment operator are available; the copy
constructor and assignment operator are not available.
<p>
<h2 >OVERLOADED OPERATORS</h2>
<p>
In the provided examples <em >tab</em> refers to an existing <em >CSVTable</em>
object. Each insertion statement (note: not insertion <em >expression</em>) either
defines or updates the table columns' definitions or fills the next row of the
table with data.
<p>
<strong >Defining column characteristics</strong>
<p>
The return types and left-hand side operands of the following insertion
operators are specified as <em >CSVTabDef</em>. The member <em >fmt()</em> (cf. section
<strong >MEMBER FUNCTIONS</strong>) returns a <em >CSVTabDef</em> object which is then used in
combination with the following insertion operators to define the
characteristics of the table's columns.
<p>
<ul>
    <li> <strong >CSVTabDef &amp;operator&lt;&lt;(CSVTabDef &amp;tab, FMT const &amp;fmt)</strong>:<br/>
       This insertion operator defines the characteristics of the next table
        column. <em >FMT</em> objects inserted into <em >CSVTabDef</em> objects must have
        been returned by <em >center, left</em> or <em >right</em> (see section <strong >FREE
        FUNCTIONS</strong>, below), or an exception will be thrown. When redefining
        column specifications (e.g., when inserting <em >FMT</em> objects for
        previously defined columns) then the width of the wider column is
        used. Example:
    <pre>

                // left align using 10 char. positions:
    tab.fmt() &lt;&lt; FBB::left(10);
                // 1st col now right aligned, but its
                // width remains 10
    tab.fmr() &lt;&lt; FBB::right(4);
    
</pre>

<p>
<li> <strong >CSVTabDef &amp;operator&lt;&lt;(CSVTabDef &amp;tab, Type const &amp;value)</strong>:<br/>
       This operator is defined for the template type <em >Type</em> parameter
        <em >value</em>, where <em >Type</em> values must be insertable in
        <em >std::ostreams</em>. The (trimmed) width of <em >value</em> when inserted into
        an <em >ostream</em> defines the width of the next column, which is
        right-aligned. As width the previous insertion operator: if a previous
        definition specified a larger width, then that width is kept. Example:
       <pre>

                // 2 columns, having widths 2 and 5:
    tab.fmt() &lt;&lt; 12 &lt;&lt; "hello";
       
</pre>

    </ul>
<p>
<strong >Inserting table elements</strong>
<p>
In addition to the insertion operator actually inserting a value into the next
table's column(s) several format modifying insertion operators are
available. When a series of specifications are inserted before the actual
value is inserted then the specification inserted just before inserting the
table's value is used, overruling that column's default specification. Format
specifications other than those provided by the standard I/O manipulators are
ignored when used beyond the table's last column.
<p>
The return types and left-hand side operands of the following insertion
operators use <em >CSVTabIns</em> objects. <em >CSVTable's</em> conversion operator
<em >operator CSVTabIns()</em> described below returns a <em >CSVTabIns</em> object which
is used by the following insertion operators to insert values into the table.
<p>
<ul>
    <li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, FMT::hline)</strong>:<br/>
       This operator inserts a horizontal line in the table's next column
        element. It is ignored when used beyond the table's last column;
<p>
<li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, (*FMT::hline)(unsigned
        nColumns))</strong>:<br/>
       This operator inserts a horizontal line spanning the next <em >nColumns</em>
        columns of the table. If the argument <em >nColumns</em> is omitted then a
        horizontal line is inserted spanning all of the table's remaining
        columns. When covering multiple columns no separators are used between
        the columns containing horizontal lines but one continuous horizontal
        line is used instead. The horizontal line is never written beyond the
        table's last column.
<p>
<li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, Type const &amp;value)</strong>:<br/>
       This operator is defined for the template type <em >Type</em> parameter
        <em >value</em>, where <em >Type</em> values must be insertable in
        <em >std::ostreams</em>. The value is inserted into the next table column,
        using the format specification that's active for that column. However,
        the specifications may be altered just before inserting the
        value. Values inserted beyond the table's last column are inserted
        as-is (although standard <em >I/O</em> manipulators can still be used);
<p>
<li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, FMT const &amp;fmt)</strong>:<br/>
       <em >FMT</em> objects are returned by several free functions defined in the
        <em >FBB</em> namespace (i.e., <em >center, left,</em> or <em >right</em>, described
        below in section <strong >FREE FUNCTIONS</strong>). Example:
    <pre>

            // left align using precision 2. E.g.,
            // e.g., '12.13     '
    tab &lt;&lt; left(2) &lt;&lt; 12.1278;
    
</pre>

<p>
<li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, FMT::Align align)</strong>:<br/>
       The alignment argument can be <em >FMT::CENTER, FMT::LEFT</em> or
        <em >FMT::RIGHT</em>. Example:
    <pre>

            // centers '12' in its column,
            // e.g., '    12    '
    tab &lt;&lt; FMT::CENTER &lt;&lt; 12;
    
</pre>

<p>
<li> <strong >void operator&lt;&lt;(CSVTabIns &amp;tab,
                        std::ios_base &amp;(*func)(std::ios_base &amp;))</strong>:<br/>
       This insertion operator accepts manipulators like <em >std::left</em> and
        <em >std::right</em>. When inserting these manipulators the next value to
        insert into the table is manipulated accordingly, overruling the next
        column's default specification.  Example:
       <pre>

            // 'hi' is left-aligned, using the
            // using the default width and precision
    tab &lt;&lt; std::left &lt;&lt; "hi";
       
</pre>

<p>
<li> <strong >CSVTabIns &amp;operator&lt;&lt;(CSVTabIns &amp;tab, Sep const &amp;sep)</strong>:<br/>
       The separator used when starting to insert values into the table's next
        row is changed to the separator specified by <em >sep</em>. It remains
        active for the table's current row, also when inserting values beyond
        the table's last column. Example:
       <pre>

            // writes, e.g., 'one, hi  there'
    tab &lt;&lt; "one" &lt;&lt; FMT::Sep{" "} &lt;&lt; "hi" &lt;&lt; "there";
       
</pre>

<p>
<li> <strong >operator CSVTabIns()</strong>:<br/>
       The conversion operator returns a <em >CSVTabIns</em> object which is used in
        combination with the above insertion operators to insert values into
        the next row of the table. Normally insertions start at column 1, but
        when called after calling <em >tab.more</em> (see below) then insertions
        continue after the last element that was inserted into
        <em >tab.more</em>. Each time this conversion operator is used another row
        is added to the table. Insertions beyond the table's last column are
        processed, but <em >CSVTabIns's</em> insertion operators are ignored,
        inserting values as-is. However, in that case the standard
        <em >std::ostream</em> manipulators can also be used;
<p>
<li> <strong >void operator()(std::string const &amp;text)</strong>:<br/>
       Calls <em >
<tr >
text, 0 
</tr>
</em> to insert the trimmed comma-separated elements
        of <em >text</em> into the table's next row;
<p>
<li> <strong >FMT const &amp;operator[](unsigned idx) const</strong>:<br/>
       Returns the default <em >FMT</em> specification of column <em >idx</em> (see also
        the description of the member <em >size()</em> below).
    </ul>
<p>
<h2 >MEMBER FUNCTIONS</h2>
<p>
In the provided examples <em >tab</em> refers to an existing <em >CSVTable</em>
    object.
<p>
<ul>
    <li> <strong >std::vector&lt;FMT&gt; const &amp;columns() const</strong>:<br/>
       Returns a reference to the vector containing the format specifications
        of the table managed by <em >CSVTable</em>;
<p>
<li> <strong >CSVTabDef &amp;fmt(unsigned idx = 0)</strong>:<br/>
       The elements inserted into the <em >CSVTabDef</em> object returned by
        <em >fmt()</em> define the specifications of the table's columns.
        Specifications start at column offset <em >idx</em>, using 0 if not
        specified (its argument may not exceed the number of already defined
        columns or an exception is thrown). When called repeatedly for already
        specified columns then the widths of existing columns are kept if they
        exceed the widths of the corresponding inserted <em >FMT</em>
        elements. Repeated <em >fmt</em> calls may specify more columns than
        previous calls, in which case new columns are added to the table;
<p>
<li> <strong >void fmt(std::string const &amp;colSpecs, unsigned idx = 0)</strong>:<br/>
       The comma-separated space-trimmed words of <em >colSpecs</em> define the
        widths of right-aligned table columns, starting at column index
        <em >idx</em>, using 0 if not specified (its argument may not exceed the
        number of already defined columns or an exception is thrown). When
        called repeatedly for already specified columns then the widths of
        existing columns are kept if they exceed the lengths of the
        corresponding trimmed words. Repeated calls may specify more columns
        than previous calls, in which case additional columns are added to the
        table. Example:
       <pre>

            // Define three right-aligned columns,
            // having widths of 3, 3 and 5.
    tab.fmt("one, two, three");
            // add columns 4 thru 6
    tab.fmt("one, two, three", 3);
       
</pre>

<p>
<li> <strong >unsigned idx() const</strong>:<br/>
       The index of the column that will be used at the next insertion is
        returned. When inserting more values than the number of defined table
        columns then the return value of the member <em >size</em> is returned;
<p>
<li> <strong >CSVTabIns more(unsigned idx = ~0U)</strong>:<br/>
       When the default <em >idx</em> argument is used then values that are inserted
        into the returned <em >CSVTabIns</em> object are inserted beyond the
        last-used column of the table's current row (which may be the row's
        first element).
<p>
When using another argument then insertions start in column <em >idx</em>. If
        <em >dx</em> exceeds the last-used column index then intermediate columns
        remain empty.
<p>
If <em >idx</em> is less than the column index that is used at the next
        insertion an exception is thrown.
<p>
Insertions beyond the table's last column are processed, but then
        <em >CSVTabIns's</em> insertion operators are ignored, inserting values
        as-is. However, in that case the standard <em >std::ostream</em>
        manipulators can also be used;
<p>
Following <em >more</em> the current row doesn't end, but values inserted
        next are inserted into the same row. Example:
       <pre>

            // a row containing one element:
    tab &lt;&lt; 1;
            // the next row contains 2 elements:
    tab.more() &lt;&lt; 1 &lt;&lt; 2;
            // now containing 4 elements
            // (element at idx 2 remains empty):
    tab.more(3) &lt;&lt; 4;
            // completes the row, now having
            // 5 elements:
    tab &lt;&lt; 5;
       
</pre>

<p>
Following <em >more</em> calls the current row ends at the next <em >tab.row</em>
        call. If following <em >more</em> calls the current row should merely end
        then simply use <em >tab.row()</em>;
<p>
<li> <strong >void more(std::string const &amp;text, unsigned idx = ~0U)</strong>:<br/>
       This member's <em >idx</em> parameter is handled as described at the previous
        member.
<p>
The trimmed comma-separated elements of <em >text</em> are inserted into the
        current row, without ending the current row;
<p>
<li> <strong >CSVTabIns row(unsigned idx = ~0U)</strong>:<br/>
       This member's <em >idx</em> parameter and insertions into the returned
        <em >CSVTabIns</em> object are handled as described at the first <em >more</em>
        member, but the current row ends at the end of the statement. Example:
       <pre>

            // a row containing one element:
    tab &lt;&lt; 1;
            // the next row contains 2 elements:
    tab.more() &lt;&lt; 1 &lt;&lt; 2;
            // the now contains 4 elements
            // (element at idx 2 remains empty):
    tab.row(3) &lt;&lt; 4;
       
</pre>

<p>
<li> <strong >void row(std::string const &amp;text, unsigned idx = ~0U)</strong>:<br/>
       This member's <em >idx</em> parameter is handled as described at the first
        <em >more</em> member.
<p>
The trimmed comma-separated elements of <em >text</em> are inserted into the
        current row, whereafter the row ends;
<p>
<li> <strong >void  stream(std::ostream &amp;out)</strong>:<br/>
       After calling <em >tab.stream(out)</em> the table's construction continues
        at the next row using the stream <em >out</em>;
<p>
<li> <strong >void stream(std::ofstream &amp;&amp;tmp)</strong>:<br/>
       After calling this member the table's construction continues at the
        next row using the <em >ofstream tmp</em>, whih is grabbed by <em >CSVTable</em>;
<p>
<li> <strong >void stream(std::string const &amp;fname,
                    std::ios::openmode mode = std::ios::out)</strong>:<br/>
       After calling this member the table's construction continues at the
        next row using the (path)name <em >fname</em>, by default (re)writing the
        file. If the file already exists and <em >CSVTable</em> should start writing
        at the file's end use, e.g., <em >ios::ate | ios::in</em>. An exception is
        thrown if the specified file cannot be opened;
<p>
<li> <strong >std::ostream &amp;stream()</strong>:<br/>
       A reference to the currently used stream is returned;
<p>
<li> <strong >std::string const &amp;sep() const</strong>:<br/>
       Returns the currently used default column separator;
<p>
<li> <strong >void sep(std::string const &amp;separator)</strong>:<br/>
       Changes the currently used default column separator to
        <em >separator</em>;
<p>
<li> <strong >unsigned size() const</strong>:<br/>
       The number of defined columns is returned.
    </ul>
<p>
<h2 >FREE FUNCTIONS</h2>
<p>
In the following examples <em >tab.fmt()</em> refers to a <em >CSVTabDef</em> object.
<p>
<strong >Defining Column Characteristics</strong>
<p>
The following functions are used to specify the alignment, width and optional
precision of columns. The first argument of these functions specifies the
column's width, the second argument is optional and specifies the column's
precision (used when inserting floating point values). The precision is only
used if its value is less than the column's width.
<p>
<ul>
    <li> <strong >FMT center(unsigned width, unsigned precision = ~0U)</strong>:<br/>
       When inserting this function's return value into <em >tab.fmt()</em> the
        values inserted into its column are centered in fields of <em >width</em>
        characters wide. Example:
       <pre>

            // values are centered in fields of 10
            // characters wide, floating point values
            // use 3 digit behind the decimal point:
    tab.fmt() &lt;&lt; center(10, 3);
       
</pre>

<p>
<li> <strong >FMT center(std::string const &amp;str, unsigned precision = ~0U)</strong>:<br/>
       A convenience function calling <em ><div style="text-align: center">str.length(), precision</div></em>;
<p>
<li> <strong >FMT left(unsigned width, unsigned precision = ~0U)</strong>:<br/>
       When inserting this function's return value into <em >tab.fmt()</em> the
        values inserted into its column are left-aligned in fields of
        <em >width</em> characters wide. Example:
       <pre>

            // values are left-aligned in fields
            // of 5 characters wide.
    tab.fmt() &lt;&lt; left(5);
       
</pre>

    <li> <strong >FMT left(std::string const &amp;str, unsigned precision = ~0U)</strong>:<br/>
       A convenience function calling <em >left(str.length(), precision)</em>;
<p>
<li> <strong >FMT right(unsigned width, unsigned precision = ~0U)</strong>:<br/>
       When inserting this function's return value into <em >tab.fmt()</em> the
        values inserted into its column are right-aligned in fields of
        <em >width</em> characters wide. Example:
       <pre>

            // values are right-aligned in fields
            // of 5 characters wide.
    tab.fmt() &lt;&lt; right(5);
       
</pre>

       Right-alignment is also used when using <em >CSVTab's fmt(std::string)</em>
        member or when directly inserting values into <em >CSVTabDef</em> objects;
<p>
<li> <strong >FMT right(std::string const &amp;str, unsigned precision = ~0U)</strong>:<br/>
       A convenience function calling <em >right(str.length(), precision)</em>.
    </ul>
<p>
<strong >Inserting Table Elements</strong>
<p>
In the following examples <em >tab</em> refers to a <em >CSVTable</em> object returning a
<em >CSVTabIns</em> object using its conversion operator.
<p>
Except for the function <em >hline</em> the following functions are used to alter
the column's default alignment and precision. The precision is only used if
its value is less than the column's width. By specifying <em >~0U</em> the precision
is ignored. If only the default alignment should be overruled then inserting
the corresponding <em >FMT::Align</em> value suffices.
<p>
Altering the default alignment of individual columns:
<p>
<ul>
    <li> <strong >FMT <div style="text-align: center">precision</div></strong>:<br/>
       After inserting this function's return value into <em >tab</em> the value
        inserted next is centered, using <em >precision</em> when inserting floating
        point values.
       <pre>

            // centers 9.87 in column 1
    tab &lt;&lt; center(2) &lt;&lt; 9.876";
       
</pre>

<p>
<li> <strong >FMT left(precision)</strong>:<br/>
       After inserting this function's return value into <em >tab</em> the value
        inserted next is left-aligned, using <em >precision</em> when inserting
        floating point values.
       <pre>

            // left-aligns 9.87 in column 1
    tab &lt;&lt; left(2) &lt;&lt; 9.876";
       
</pre>

<p>
<li> <strong >FMT right(precision)</strong>:<br/>
       When inserting this function's return value into <em >tab</em> the value
        inserted next is right-aligned, using <em >precision</em> when inserting
        floating point values.
       <pre>

            // right-aligns 9.87 in column 1
    tab &lt;&lt; right(2) &lt;&lt; 9.876";
       
</pre>

       By default <em >CSVTable</em> uses right-alignment.
    </ul>
<p>
<strong >Joining columns:</strong>
<p>
Alignments specifications may span multiple columns. This is realized through
the <em >join</em> functions. When inserting a value after inserting the return
value of a <em >join</em> member then that value is inserted occupying all the
columns and using the alignment type specified when calling <em >join</em>.  If
necessary the number of columns is reduced to avoid exceeding the table's last
column.
<p>
<ul>
    <li> <strong >FMT join(unsigned nCols, FMT::Align align, unsigned precision = ~0U)</strong>:<br/>
       A value that's inserted into the table after inserting <em >join's</em>
        return value occupies <em >nCols</em> columns, using alignment type
        <em >align</em>, and optionally using <em >precision</em> when inserting floating
        point values. The alignment specification must be <em >FMT::CENTER,
        FMT::LEFT</em> or <em >FMT::RIGHT</em>. Example:
    <pre>

            // writes (assuming columns 2 and 3 occupy
            // 10 characters):
            //      left,    mid    , right
    tab &lt;&lt; "left" &lt;&lt; join(2, FMT::CENTER) &lt;&lt; "mid" &lt;&lt; "right"";
    
</pre>

<p>
<li> <strong >FMT join(FMT::Align align, unsigned precision = ~0U)</strong>:<br/>
       Same effect as the previous <em >join</em> function, but this function
        occupies all remaining columns of the table's current row (this can
        also be accomplished by calling the first <em >join</em> function specifying
        <em >~0U</em> as its first argument).
    </ul>
<p>
<strong >Inserting horizontal lines:</strong>
<p>
If a single table element should contain a horizontal line then simply
inserting <em >Align::HLINE</em> works fine. The <em >hline</em> functions
are used to insert horizontal lines spanning one or more table columns.
<p>
<ul>
    <li> <strong >FMT hline(unsigned nCols = ~0U)</strong>:<br/>
       When inserting this function's return value into a <em >CSVTabIns</em> object
        a horizontal line spanning <em >nCols</em> columns is inserted into the
        table. If necessary <em >nCols</em> is reduced so that the horizontal line
        does not exceed the table's last column. When spanning multiple
        columns no column separated are used between the spanned columns: a
        single uninterrupted horizontal line is inserted. Example:
       <pre>

            // columns 1 and 2: a horizontal line, column 3:
            // contains 'hi' (preceded by the column separator)
    tab &lt;&lt; hline(2) &lt;&lt; "hi";
        
</pre>

    </ul>
<p>
<h2 >EXAMPLE</h2>
    <pre>

#include &lt;bobcat/csvtable&gt;

using namespace FBB;

int main()
{
    CSVTable tab;

    tab.fmt() &lt;&lt; "case" &lt;&lt; right("length", 2) &lt;&lt; right("weight", 1) &lt;&lt;
                           right("length", 2) &lt;&lt; right("weight", 1);
    tab.sep("  ");

    tab &lt;&lt; hline();
    tab &lt;&lt; "" &lt;&lt; join(4, FMT::CENTER) &lt;&lt; "Gender";
    tab &lt;&lt; "" &lt;&lt; hline();

    tab &lt;&lt; "" &lt;&lt; join(2, FMT::CENTER) &lt;&lt; "Female" &lt;&lt;
                 join(2, FMT::CENTER) &lt;&lt; "Male";
    tab &lt;&lt; "" &lt;&lt; hline(2) &lt;&lt; hline(2);

    tab &lt;&lt; "Case" &lt;&lt; "Length" &lt;&lt; "Weight" &lt;&lt; "Length" &lt;&lt; "Weight";
    tab &lt;&lt; hline();

    tab &lt;&lt; 1 &lt;&lt; 1.744 &lt;&lt; 55.345 &lt;&lt; 1.7244 &lt;&lt; 64.801;
    tab &lt;&lt; 2 &lt;&lt; 1.58  &lt;&lt; 57.545 &lt;&lt; 1.8174 &lt;&lt; 81.451;
    tab &lt;&lt; 3 &lt;&lt; 1.674 &lt;&lt; 62.125 &lt;&lt; 1.8244 &lt;&lt; 80.201;

    tab &lt;&lt; hline();
}
    
</pre>

<p>
This program writes the following table to <em >std::cout</em>:
    <pre>

------------------------------------
                  Gender
      ------------------------------
          Female           Male
      --------------  --------------
Case  Length  Weight  Length  Weight
------------------------------------
   1    1.74    55.3    1.72    64.8
   2    1.58    57.5    1.82    81.5
   3    1.67    62.1    1.82    80.2
------------------------------------
    
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/csvtable</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >table</strong>(3bobcat)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
