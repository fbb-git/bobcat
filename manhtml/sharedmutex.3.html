<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::SharedMutex(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::SharedMutex(3bobcat)</h1>
<h2 id="author">Shared Memory Mutex<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::SharedMutex - Mutex for shared memory
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/sharedmutex&gt;</strong><br/>
    Linking option: <em >-lpthread, -lbobcat </em>
<p>
<h2 >DESCRIPTION</h2>
    Shared memory may be used by multiple processes. To synchronize access to
shared memory an <strong >FBB::SharedMutex</strong> may be defined inside a shared memory
segment. <strong >SharedMutex</strong> objects allows clients to lock a shared memory
segment before reading or writing its content. E.g., the Bobcat class
<em >FBB::SharedSegment</em> defines a <strong >SharedMutex</strong> in its shared memory segment.
<p>
The <strong >SharedMutex</strong> class uses the facilities offered by the
<em >PThread</em> library to implement (non recursive) shared memory locking. To
force unlocking a (possibly) locked shared memory segment, its destructor can
be called.
<p>
<strong >SharedMutex</strong> mutexes are non-recursive, resulting in deadlocks if their
<em >lock</em> member is called twice from the same thread of execution without an
intermediate call to <em >unlock</em> the mutex. If this causes concern, a variable
can be defined indicating whether the lock has already been obtained.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    -
<p>
<h2 >CONSTRUCTORS, DESTRUCTOR </h2>
    <ul>
    <li> <strong >SharedMutex()</strong>:<br/>
       The default constructor initializes an <strong >FBB::SharedMutex</strong> object to a
        shared memory mutex (using the <em >PTHREAD_PROCESS_SHARED</em>
        attribute). As an <strong >FBB::SharedMutex</strong> object will normally be defined
        inside a shared memory segment the object's memory is already
        available. In this case placement new should be used to call the
        constructor. E.g., if a shared memory segment is attached to the
        current process at <em >d_shared</em>, and an <strong >FBB::SharedMutex</strong> should be
        defined at <em >d_shared</em>'s address, then the <strong >FBB::SharedMutex</strong>
        object can be initialized like this:
       <pre>

    new (d_shared) FBB::SharedMutex;
       
</pre>

       Caveat: when using placement new to initialize a <strong >SharedMutex</strong> make
        sure that the <strong >SharedMutex</strong> fits inside a block (i.e.,
        <em >shared.blockOffset() + sizeof(SharedMemory) &lt;
        shared.dataSegmentSize()</em>). If not, use <em >seek</em> to switch to an
        offset where this equality holds true, or simply use
        <em >SharedMemory::create</em> like this:
       <pre>

    FBB::SharedMemory::create&lt;FBB::SharedMutex&gt;();
       
</pre>

<p>
<li> <strong >~SharedMutex()</strong>:<br/>
       The class's destructor releases all of the current process's nested
        shared memory segment locks. To destroy an <strong >FBB::SharedMutex</strong> object
        that has been constructed using the placement <em >new</em> operator use
       <pre>

d_sharedMutex-&gt;~SharedMutex();
       
</pre>

       (assuming <em >SharedMutex *d_sharedMutex</em> points to the location where
        placement new has previously initialized the <strong >FBB::SharedMutex</strong>
        object.)
<p>
</ul>
<p>
Copy and move constructors (and assignment operators) are not available.
<p>
<h2 >MEMBER FUNCTIONS</h2>
    <ul>
    <li> <strong >void lock() const</strong>:<br/>
       When returning from this member, the current process has locked the
        shared memory segment. Note that <strong >SharedMutex</strong> objects are
        non-recursive.
<p>
<li> <strong >void unlock() const</strong>:<br/>
       The object's lock of the shared memory segment is released. This member
        can also be called if the <strong >SharedMutex's</strong> lock has not been
        obtained.
    </ul>
<p>
<h2 >PROTECTED MEMBER FUNCTION</h2>
    <ul>
    <li> <strong >pthread_mutex_t *mutexPtr()</strong>:<br/>
       A pointer is returned to the <em >pthread_mutex_t</em> object used by
        the <strong >SharedMutex</strong> object;
    </ul>
<p>
<h2 >EXAMPLE</h2>
<p>
<pre >
#include &lt;sys/types.h&gt;
#include &lt;signal.h&gt;

#include &lt;iostream&gt;
#include &lt;string&gt;
#include &lt;chrono&gt;
#include &lt;bobcat/fork&gt;
#include &lt;bobcat/semaphore&gt;
#include &lt;bobcat/sharedsegment&gt;
#include &lt;bobcat/sharedmutex&gt;

using namespace std;
using namespace FBB;

class Wait: public Fork
{
    SharedSegment *d_shared;
    int d_id;
    SharedMutex *d_mutex;

    public:
        Wait();
        ~Wait();
        void childProcess() override;
        void parentProcess() override;
};

Wait::Wait()
:
    d_shared(SharedSegment::create(&amp;d_id, 1, 100, 0700)),
    d_mutex(new (d_shared) SharedMutex)
{
    cout &lt;&lt; "shared memory ID = " &lt;&lt; d_id &lt;&lt; '\n';
}

Wait::~Wait()
{
    d_mutex-&gt;~SharedMutex();
    SharedSegment::deleteSegment(d_id);
    cout &lt;&lt; "deleted the shared memory\n";
}

void Wait::childProcess()
{
    Semaphore waiter{0};

    while (true)
    {
        waiter.wait_for(chrono::seconds(2));
        d_mutex-&gt;lock();
        cout &lt;&lt; "child hello\n";
        d_mutex-&gt;unlock();
    }
}

void Wait::parentProcess()
{
    string line;
    do
    {
        cout &lt;&lt; "press enter to allow the parent to locck\n";
        cin.ignore(100, '\n');
        d_mutex-&gt;lock();
        cout &lt;&lt; "parent has the lock, press enter to continue "
                "(to end: some input)\n";
        getline(cin, line);
        d_mutex-&gt;unlock();
    }
    while (line.empty());

    kill(pid(), SIGTERM);
}


int main()
{
    Wait waiter;
    waiter.fork();
}
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/sharedmutex</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7)
        <strong >isharedstream</strong>(3bobcat),
        <strong >osharedstream</strong>(3bobcat),
        <strong >sharedblock</strong>(3bobcat),
        <strong >sharedcondition</strong>(3bobcat),
        <strong >sharedmemory</strong>(3bobcat),
            (e.g.,) <strong >pthread_mutex_init</strong>(3posix),
        <strong >sharedpos</strong>(3bobcat),
        <strong >sharedreadme</strong>(7bobcat),
        <strong >sharedsegment</strong>(3bobcat),
        <strong >sharedstream</strong>(3bobcat),
        <strong >sharedbuf</strong>(3bobcat)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
