<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::OMutexStream(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::OMutexStream(3bobcat)</h1>
<h2 id="author">Mutex protected std::ostream<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::OMutexStream - Offers mutex protected std::ostream facilities
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/omutexstream&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
<p>
In multi-threaded programs output written by different threads to the same
output stream may be intermixed due to context switches. This is prevented by
using mutexes: before writing to the output stream a mutex lock is acquired,
releasing the lock once the output is completed.
<p>
<em >OMutexStream</em> supports writing to output streams while handling the mutex
locks. The <em >OMutexStream</em> object itself is initialized with the
<em >std::ostream</em> object to receive the information written to the
<em >OMutexStream</em> object. This object, like <em >std::cout</em> and <em >std::err</em>
usually is defined as a globally accessible object. When inserting information
into the <em >OMutexStream</em> object <em >OMutexStream</em> first returns an
<em >OMutexStream::Out</em> object, whose constructor locks the
<em >OMutexStream::Out</em> mutex. The <em >OMutexStream::Out</em> object's lifetime ends
at the end of the insertion statement, and at that time its destructor
releases the lock.
<p>
In many cases this is appropriate. Especially when statements end in newlines
(<em >'\n'</em> or <em >endl</em>) this results in clean output. In some cases this
approach doesn't work. Consider the situation where the output is produced by
a series of iterations in a <em >for</em>-statement. For these cases
<em >OMutexStream</em> offers the member <em >ostream</em> returning an
<em >OMutexStream::Out</em> object. As that object also locks the mutex, the lock
also remains active during its lifetime. During its lifetime separate
<em >OMutexStream</em> insertions expressions may be executed. E.g., the following
code fragment will complete all output forcing competing threads to wait:
        <pre>

    void fun()
    {
        OMutexStream mout{ std::cout }; // create an OMutexStream object
        {
            auto out{ mout.ostream() }  // locks the mutex (lock #1)
            mout &lt;&lt; "Hello ";           // also locks (lock #2, at ;
                                        //             back to lock #1)
            out &lt;&lt; "world\n";
        }                               // 'out' releases the lock
    }
        
</pre>

    Be careful to restrict the lifetime of the object returned by
<em >OMutexStream::ostream()</em>.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    (via <em >OMutexStream::Out</em>) <em >std::ostream</em>
<p>
<h2 >CONSTRUCTORS</h2>
    <ul>
    <li> <strong >OMutexStream(std::ostream &amp;out)</strong>:<br/>
       The <em >OMutexStream</em> object is initialized with an <em >std::ostream</em>
        object.
    </ul>
<p>
Copy and move constructors (and assignment operators) are available.
<p>
<h2 >OVERLOADED OPERATORS</h2>
    <ul>
    <li> <strong >OMutexStream::Out operator&lt;&lt;(OMutexStream const &amp;mstr, Tp &amp;&amp;tp)</strong>:<br/>
       This member is a function template. Its forwarding reference is passed
        on to the <em >OMutexStream::Out</em> object constructed by <em >mstr</em>. Since
        <em >OMutexStream::Out</em> constructors lock the class's mutex, and since
        <em >OMutexStream::Out</em> was derived from <em >std::ostream</em> all insertion
        and other operations that are allowed for <em >std::ostream</em> can also be
        used for <em >OMutexStream::Out</em> objects.
<p>
<li> <strong >OMutexStream::Out operator&lt;&lt;(OMutexStream const &amp;mstr,
                                                    Ret &amp;(*manip)(Ret &amp;os))</strong>:<br/>
       This member also is a function template. It is used for inserting
        manipulators without arguments into <em >OMutexStream::Out</em> objects.
    </ul>
<p>
<h2 >MEMBER FUNCTIONS</h2>
    <ul>
    <li> <strong >OMutexStream::Out ostream() const</strong>:<br/>
       A <em >OMutexStream::Out</em> object is returned that has locked its mutex,
        and will keep the lock during its lifetime. All functionality of the
        <em >std::ostream</em> class can also be used for the <em >OMutexStream::Out</em>
        object returned by <em >ostream</em>.
<p>
Be careful to restrict the lifetime of the object returned by
        <em >OMutexStream::ostream()</em> to avoid needlessly long mutex locks.
    </ul>
<p>
<h2 >EXAMPLE</h2>
    <pre >
#include &lt;iostream&gt;
#include &lt;thread&gt;
#include &lt;chrono&gt;
#include &lt;cstdlib&gt;
#include &lt;cmath&gt;
#include &lt;ctime&gt;

#include &lt;bobcat/omutexstream&gt;

using namespace std;
using namespace FBB;

OMutexStream mout(cout);

void run(int nr)
{
    for (size_t idx = 0; idx != 3; ++idx)
    {
        mout &lt;&lt; "hello world 1 from thread " &lt;&lt; nr &lt;&lt; ": " &lt;&lt;
                log(rand()) &lt;&lt; endl;

        this_thread::sleep_for(
                chrono::milliseconds(200 + rand() % 800));

       mout &lt;&lt; "hello world 2 from thread " &lt;&lt; nr &lt;&lt; ": " &lt;&lt;
                log(rand()) &lt;&lt; '\n';

        this_thread::sleep_for(
                chrono::milliseconds(200 + rand() % 800));
    }

    auto out{ mout.ostream() };
    cout &lt;&lt; nr &lt;&lt; ": " &lt;&lt; out.tellp() &lt;&lt; '\n';
}

int main()
{
    srand(time(0));

    thread t1(run, 1);
    thread t2(run, 2);
    thread t3(run, 3);
    thread t4(run, 4);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
}
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/omutexstream</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
