<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::IFdBuf(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::IFdBuf(3bobcat)</h1>
<h2 id="author">File Descriptor Input Stream Buffer<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::IFdBuf - Input stream buffer initialized by a file descriptor
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/ifdbuf&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
    <strong >FBB::IFdBuf</strong> objects may be used as a <em >std::streambuf</em> of
<em >std::istream</em> objects to allow extractions from a file descriptor.
<p>
File descriptors are not defined within the context of <strong >C++</strong>,
but they can be used on operating systems that support the concept. Realize
that using file descriptors introduces operating system dependencies.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    <strong >std::streambuf</strong>
<p>
<h2 >ENUMERATION</h2>
    The public enumeration <em >Mode</em> defined in the class <em >FBB::IFdStreamBuf</em>
has the following values:
    <ul>
    <li> <strong >CLOSE_FD</strong>, indicating that the file descriptor used by the object
must be closed;
    <li> <strong >KEEP_FD</strong> (the default) indicating that the file descriptor used by
the object must not be closed.
    </ul>
<p>
<h2 >CONSTRUCTORS</h2>
    <ul>
    <li> <strong >IFdBuf()</strong>:<br/>
        This constructor initializes the streambuf, without associating it to
a file descriptor, and without using buffering. The member <em >reset</em> can be
used subsequently to associate the object with a file descriptor and
optionally a buffer size. When the object is destroyed or if the mode-less
overloaded version of the <em >reset</em> member is called, the file descriptor is
not closed.
<p>
<li> <strong >IFdBuf(Mode mode)</strong>:<br/>
        This constructor initializes the streambuf, without associating it to
a file descriptor, and without using buffering. The member <em >reset</em> can be
used subsequently to associate the object with a file descriptor and
optionally a buffer size. When the object is destroyed or if the mode-less
overloaded version of the member <em >reset</em> is called, the <em >Mode</em> argument
determines whether the file descriptor will be closed or will remain open.
<p>
<li> <strong >IFdBuf(int fd, size_t n = 1)</strong>:<br/>
        This constructor initializes the streambuf, associating it to file
descriptor <em >fd</em>, and an optional unget buffer size (by default having size
1).  When the object is destroyed or if the mode-less overloaded version of
the member <em >reset</em> is called, the file descriptor is not closed.
<p>
<li> <strong >IFdBuf(int fd, Mode mode, size_t n = 1)</strong>:<br/>
        This constructor initializes the streambuf, associating it to file
descriptor <em >fd</em>, and an optional unget buffer size (by default having size
1).   When the object is destroyed or if the mode-less
overloaded version of the <em >reset</em> member is called, the <em >Mode</em> argument
determines whether the file descriptor will be closed or will remain open.
    </ul>
<p>
Copy and move constructors (and assignment operators) are not available.
<p>
<h2 >MEMBER FUNCTIONS</h2>
     All members of <em >std::streambuf</em> are
available, as <em >FBB::IFdBuf</em> inherits from this class.
<p>
<ul>
    <li> <strong >void close()</strong>:<br/>
        The file descriptor used by the <em >IFdBuf</em> is closed,
irrespective of the <em >Mode</em> that was specified when the <em >IFdBuf</em>
object was constructed. Following <em >close</em> the <em >IFdBuf</em> object can
no longer be used until one of its <em >reset</em> members has been called.
<p>
<li> <strong >int fd() const</strong>:<br/>
        The file descriptor used by the <em >IFdBuf</em> object is returned.
        If the <em >OFdBuf</em> is not associated with a file descriptor -1 is
        returned.
<p>
<li> <strong >void reset(int fd, size_t n = 1)</strong>:<br/>
        The streambuf is (re)initialized, using file descriptor <em >fd</em>, and an
optional unget buffer size (by default having size 1). When called repeatedly,
the <em >Mode</em> specification used whem the object was constructed determines
whether the file descriptor will be closed or will remain open.
<p>
<li> <strong >void reset(int fd, Mode mode, size_t n = 1)</strong>:<br/>
        The streambuf is (re)initialized, using file descriptor <em >fd</em>, a file
descriptor closing parameter and an optional unget buffer size (by default
having size 1). Depending on the <em >Mode</em> argument the object's currently used
file descriptor will be closed or will remain open when the <em >IFdBuf</em>
object is destroyed.
    </ul>
<p>
<h2 >EXAMPLE</h2>
<p>
<pre >
#include &lt;unistd.h&gt;

#include &lt;istream&gt;
#include &lt;iostream&gt;

#include &lt;bobcat/ifdbuf&gt;

using namespace std;
using namespace FBB;

int main(int argc, char **argv)
{
                           // define a streambuf of 20 or argv[1] characters
    IFdBuf buf{ STDIN_FILENO, argc == 1 ? 20 : stoul(argv[1]) };

    istream in{ &amp;buf };

    cout &lt;&lt; in.rdbuf();
}
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/ifdbuf</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >ifdstream</strong>(3bobcat), <strong >ofdbuf</strong>(3bobcat),
    <strong >std::streambuf</strong>
<p>
<h2 >BUGS</h2>
    The member <em >xsgetn(char *dest, std::streamsize n)</em> sets
<em >istream::good()</em> to <em >false</em> when fewer bytes than <em >n</em> were read using
<em >istream::read()</em>. Also see <em >xsgetn</em>'s description.
<p>
Note that by default the provided file descriptors remain open. The
complementary class <strong >ofdbuf</strong>(3bobcat) by default <em >closes</em> the file
descriptor.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
