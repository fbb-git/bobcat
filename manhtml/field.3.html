<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::Field(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::Field(3bobcat)</h1>
<h2 id="author">Number fields<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::Field - sets and retrieves offset based number fields
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/field&gt;</strong><br/>
<p>
<h2 >DESCRIPTION</h2>
<p>
Numbers may contain offset-based sub-fields. E.g., a value like 12345 might
consist of fields 12 and 345. Such fields can be considered offset-based,
using end- and begin offsets in the number representations. In this example
345 begins at digit position 0 (indicating the least significant digit of
12345) and at digit position 3 the next field starts. Likewise, the field 12
begins at digit position 3 and has ended at digit position 5.
<p>
The <em >Field</em> class template provides facilities for retrieving and assigning
position based values of existing numeric values of (currently) at most 64
bits.
<p>
To represent such fields the following format is used:
        <pre>

    Field&lt;base, end, begin&gt;::function(argument(s))
        
</pre>

    where <em >base</em> specifies the number system's base value, <em >end</em> specifies
the (0-based) index position where the number field ends, and <em >begin</em>
specifies the index position where the number field begins. Here are two
examples, using the decimal number system:
        <pre>

    Field&lt;10, 3, 0&gt;::get(12345)    // returns 345
    Field&lt;10, 5, 3&gt;::get(12345)    // returns  12
        
</pre>

<p>
The decision to specify the <em >end</em> offset before (i.e., left of) the
<em >begin</em> offset is based on the consideration that this corresponds to the
standard way of looking at digit positions in numbers, where the end offset is
found to the left of the begin offset.
<p>
Values of fields can be retrieved, but they can also
be set: to set a field's value the following format is used:
        <pre>

    Field&lt;10, 3, 1&gt;::set(12345, 99)   // returns 12995
    Field&lt;10, 1, 0&gt;::set(12345, 0)    // returns 12450
        
</pre>

    When values are assigned to fields the maximum width of the destination
field is taken into account. When specifying 9999 instead of 99 in
the above example the returned value will still be 12995, as the destination
field has a width of two digit positions. Likewise, specifying a smaller value
sets the remaining (more significant) digits to 0:
        <pre>

    Field&lt;10, 3, 1&gt;::set(12345, 9)    // returns 12095
        
</pre>

<p>
The class templates themselves are unaware of bases of number systems. Since
0xdeaf equals the decimal value 57007 and 0xd equals 13, calling the above
function as
        <pre>

    Field&lt;16, 1, 0&gt;::set(76007, 13)
        
</pre>

    returns the hexadecimal value <em >0xdead'</em>.
<p>
The <em >Field</em> class template requires three non-type numeric arguments:
    <ul>
    <li> <em >base</em>, specifying the base of the number system;
    <li> <em >end</em>, specifying the 0-based offset of the digit position where the
        field has ended;
    <li> <em >begin</em>, specifying the 0-based offset of the digit position where the
        field begins;
    </ul>
<p>
The class template is specialized for situations where <em >base</em> is a mere
power of 2 (like 2, 4, 8, 16, ...) because in those cases bit-operations can
be used which are faster than multiplications, divisions and modulo
computation which are required when other number system bases are used.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    -
<p>
<h2 >MEMBER FUNCTIONS</h2>
    <ul>
    <li> <strong >uint64_t Field&lt;base, end, begin&gt;::get(uint64_t value)</strong>:<br/>
        <em >value</em> is interpreted as a value in the <em >base</em> number system, and
using digit positions in that number system the value of the digits from
offset <em >begin</em> to (but not including) offset <em >end</em> is returned;
<p>
<li> <strong >uint64_t Field&lt;base, end, begin&gt;::set(uint64_t value, uint64_t field)</strong>:<br/>
        <em >value</em> is interpreted as a value in the <em >base</em> number system, and
using digit positions in that number system the digits from
offset <em >begin</em> to (but not including) offset <em >end</em> are replaced by
<em >field's</em> value. When the number of <em >fields's</em> digits (also using number
system <em >base</em>) exceeds <em >end - begin</em> then those excess digits are
ignored.
    </ul>
<p>
<h2 >EXAMPLE</h2>
    See the examples in the <strong >DESCRIPTION</strong> section
<p>
<h2 >FILES</h2>
    <em >bobcat/field</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
