<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::ECDH(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::ECDH(3bobcat)</h1>
<h2 id="author">Elliptic Curve Diffie-Hellman<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::ECDH - Elliptic Curve Diffie-Hellman PKI, computing shared keys
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/ecdh&gt;</strong><br/>
    Linking option: <em >-lbobcat -lcrypto</em>
<p>
<h2 >DESCRIPTION</h2>
<p>
The class <strong >FBB::ECDH</strong> computes shared keys (shared secrets) applying
elliptic keys to the Diffie-Hellman (1976) algorithm. The Diffie-Hellman
algorithm uses public and private information, providing a public key
infrastructure (PKI). The public information consists of an elliptic curve
specification and a public key, which may be shared over insecure media.
<p>
The Diffie-Hellman algorithm is commonly used to compute a shared key which
is then used to encrypt information sent between two parties. 
<p>
One party, which in this man-page is called the <em >initiator</em>, specifies at
construction time an elliptic curve, constructs a public and private key, and
writes the public key in hexadecimal big-endian form, followed by the name of
the used elliptic curve to file. The initiator's private key is separately
written to file, as the private key is required for computating the shared
key. Encryption may be used when writing the private key to file.
<p>
Next the initiator passes the file containing the initiator's public key and
the name of the used elliptic curve to the other party, which in this man-page
is called the <em >peer</em>.
<p>
The peer, having received the initiator's (public key) file constructs an
<strong >FBB::ECDH</strong> object. The peer's <em >ECDH</em> constructor computes the peer's
public and private key, writes the peer's public key to file, and constructs
the shared key. Once the peer's <em >ECDH</em> object has been constructed the peer
can write the shared key to file. The peer's private key may optionally also
be written to file, but that's optional, as the peer's private key is not
required for subsequent computations. Encryption may also be used when writing
the peer's private key to file.
<p>
The file containing the peer's public key is then sent to the initator. The
initator constructs an <em >ECDH</em> object specifying the names of the
used elliptic curve, of the file containing the initiator's private key, and
the name of the file containing the peer's public key. Once this <em >ECDH</em>
object has been constructed the peer may write the shared key to file.
<p>
The initiator and peer's shared keys are identical and can be used for
symmetric encryption of sensitive information shared between the initiator and
the peer.
<p>
<em >FBB::Exception</em>s are thrown if the <em >ECDH</em> constructors  or members cannot
complete their tasks.
<p>
<strong >Perfect Forward Secrecy and Ephemeral Diffie Hellman</strong>
<p>
The initiator and peer may decide not to save their private information once
they have constructed their shared keys, resulting in <em >Perfect Forward
Secrecy</em> and <em >Ephemeral Diffie Hellman</em>. Here, this procedure is applied as
follows:
<p>
<ul>
    <li> Initiator and peer have agreed upon and securely exchanged a
long-lasting common secret, which may be used in combination with, e.g.,
symmetric encryption methods.
    <li> Applying the procedure described in the previous section, the private
keys are not saved on files, and the process constructing the initiator's
<em >ECDH</em> object may not terminate, but must remain active until the peer's
public key has been received. Once the initiator's process has constructed the
public key that key is encrypted using the common secret, and is then sent to
the peer. Alternatively, the initiator's private key may temporarily be stored
in shared memory or may temporarily be stored encrypted on file.
    <li> The peer, having received the initiator's public key, constructs the
shared secret, encrypts the peer's public key, and sends the encrypted public
key to the initiator.
    <li> The initiator upon receipt of the peer's public key, computes the
shared key, either by continuing the temporarily suspended construction
process or by retrieving the shared key from memory or file, removing the
storage (memory or file) thereafter.
    <li> Since the private keys and the public keys are not stored or kept on
files the shared keys cannot be reconstructed, while a Man-In-The-Middle
attack is prevented by only exchanging encrypted public information.
    <li> The shared key can now be used to encrypt a communication session
    </ul>
<p>
<strong >Document encryption using Diffie Hellman</strong>
<p>
As with PKI in general, the Diffie Hellman key exchange method itself is
normally not used for encrypting documents. Instead, it is used to obtain
a key that is used for symmetric encryption methods like 3DES or CBC. These
symmetric encryption methods are available through, e.g., Bobcats'
<em >ISymCryptStream</em> and <em >OSymCryptStream</em> classes.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    -
<p>
<h2 >ENUMERATIONS</h2>
    The class <em >ECDH</em> defines two enumerations, each having one defined
value. The enumberations are used to select specific overloaded versions of
the <em >ECDH</em> constructors or <em >set</em> members:
    <ul>
    <li> The t(enum TheInitiator) has value <em >Initiator</em> and is used to
        select overloaded versions meant for the initiator;
    <li> The t(enum ThePeer) has value <em >Peer</em> and is used to
        select overloaded versions meant for the peer.
    </ul>
<p>
<h2 >CONSTRUCTORS</h2>
<p>
<ul>
    <li> <strong >ECDH()</strong>:<br/>
       The default constructor is available merely constructing a valid
        object. It also prepares a map of all elliptic curves predefined by
        <em >openSSL</em>. As all other constructors use default constructor
        delegation the map is also available after calling the other
        constructors;
<p>
<li> <strong >ECDH(TheInitiator init, std::string const &amp;curveName, 
                  std::string const &amp;initPubFname)</strong>:<br/>
       This constructor initializes the <em >ECDH</em> object to be used by the
        initiator, constructing the initiator's private and public keys using
        the elliptic curve specified by <em >curveName</em> (e.g., <em >secp384r1</em>,
        see also <em >operator&lt;&lt;</em> below). The initiator's public key (in
        big-endian hexadecimal format) and <em >curveName</em> are written to
        <em >initPubFname</em>;
<p>
This constructor should be called by the initiator to start the
        Diffie-Hellman shared key computation procedure;
<p>
<li> <strong >ECDH(ThePeer peer, std::string const &amp;initPubFname,
                           std::string const &amp;peerPubFname)</strong>:<br/>
       This constructor is used by the peer, having received the initiator's
        public info on the file <em >initPubFname</em>.  It constructs the peer's
        private and public keys as well as the shared key. The peer's public
        key (in big-endian hexadecimal format) is written to <em >peerPubFname</em>,
        which file is then be sent to the initiator;
<p>
<li> <strong >ECDH(std::string const &amp;curveName, std::string const &amp;peerPubFname, 
                std::string const &amp;initSecFname,
                std::string const passphrase = "")</strong>:<br/>
       Once the initiator has received the peer's public key (in the file
        <em >peerPubFname</em>) this constructor constructs the initiator's version
        of the shared key. Here, the initiator has previously saved the
        initiator's private key to <em >initSecFname</em>, optionally using
        encryption. If encryption was used then the then used passphrase must
        also be specified when using this constructor.
    </ul>
<p>
The move constructor (and move assignment operator) is available.
<p>
<h2 >MEMBER FUNCTIONS</h2>
    <ul>
    <li> <strong >std::string const &amp;curve() const</strong>:<br/>
        The used elliptic curve is returned;
<p>
<li> <strong >std::string privKey() const</strong>:<br/> 
       The big-endian hex-formatted private key is returned, prefixed by a
        line containing <em >hex</em>;
<p>
<li> <strong >void privKey(std::string const &amp;privKeyFname,
                     std::string passphrase) const</strong>:<br/>
       The private private key is encrypted using the <em >AES-256-GCM</em>
        encryption algorithm, using passphrase <em >passphrase</em>. It is then
        written to <em >privKeyFname</em>, prefixed by a line containing
        <em >encrypted</em>. The string <em >passphrase</em> must consist of at least five
        characters, and may contain multiple words;
<p>
<li> <strong >std::string const &amp;pubKey() const</strong>:<br/>
       The public key is returned as a big-endian hexadecimal string;
<p>
<li> <strong >void set(TheInitiator init, std::string const &amp;curveName, 
                  std::string const &amp;initPubFname)</strong>:<br/>
       This member should be called by the initiator, constructing the
        initiator's private and public keys using the elliptic curve specified
        by <em >curveName</em>. The initiator's public key (in big-endian
        hexadecimal format) and <em >curveName</em> are written to <em >initPubFname</em>.
        This member is automatically called by the constructor having the same
        parameters, but it may also explicitly called after using the default
        constructor;
<p>
<li> <strong >void set(ThePeer peer, std::string const &amp;initPubFname,
                           std::string const &amp;peerPubFname)</strong>:<br/>
       This member should be called by the peer, having received the
        initiator's public info on the file <em >initPubFname</em>.  It constructs
        the peer's private and public keys as well as the shared key. The
        peer's public key (in big-endian hexadecimal format) is written to
        <em >peerPubFname</em>, which file is then be sent to the initiator.  This
        member is automatically called by the constructor having the same
        parameters, but it may also explicitly called after using the default
        constructor;
<p>
<li> <strong >void set(std::string const &amp;curveName, 
                std::string const &amp;peerPubFname,
                std::string const &amp;initSecFname, 
                std::string const passphrase = "")</strong>:<br/>
       This member should be called by the initiator, once the peer's public
        key (in the file <em >peerPubFname</em>) has been received. It computes the
        initiator's version of the shared key. When using this member the
        initiator has previously saved the initiator's private key to
        <em >initSecFname</em>, optionally using encryption. If encryption was used
        then the then used passphrase must also be specified as this member's
        last argument.
<p>
<li> <strong >std::string const &amp;sharedKey() const</strong>:<br/>
       This member returns the computed shared key (in big-endian hexadecimal
        format);
<p>
<li> <strong >std::string const &amp;sharedKey(std::string const &amp;peerPubFname)</strong>:<br/>
       Instead of using the <em >set(std::string const &amp;curveName, ...)</em> member
        or the <em >ECDH(std::string const &amp;curveName, ...)</em> constructor, the
        initiator may also merely call the <em >set(TheInitiator init, ...)</em>
        member or the <em >ECDH(TheInitiator init, ...)</em> constructor, suspending
        the process in which they are called until the file containing the
        peer's public key has been received. Then, this member can be called
        by the constructed <em >ECDH</em> object to obtain the initiator's shared
        key. The advantage of using this member is that the initiator does not
        have to save the initiator's private key.
    </ul>
<p>
<h2 >OVERLOADED OPERATOR</h2>
<p>
<ul>
    <li> <strong >std::ostream &amp;operator&lt;&lt;(ostream &amp;out, ECDH const &amp;ecdh)</strong>:<br/>
       The (alphabetically ordered) currently available elliptic curves and
        their associated comment is written to <em >out</em>, one elliptic curve on
        a separate line.
    </ul>
<p>
<h2 >EXAMPLE</h2>
<p>
Start the program with one of the following arguments:
    <ul>
    <li> curves: show the available elliptic curves on <em >cout</em>;
    <li> init: compute the initiator's public/secret keys writing them to
            <em >init.pub</em> and <em >init.sec</em>;
    <li> peer: compute the peer's public/secret keys writing them to
            <em >peer.pub</em> and <em >peer.sec</em>, compute the peer's shared key
            (<em >peer.shared</em>);
    <li> priv: compute the initiator's shared key (<em >init.shared</em>) after making
            <em >peer.pub</em> available in a separate process, using a single
            initiator process.
    <li> shared: compute the initiator's shared key (<em >init.shared</em>) using a
            separate initiator process.
    </ul>
<p>
<pre >
#include "main.ih"

int main(int argc, char **argv)
try
{
    if (argc == 1)
    {
        usage(path{ argv[0] }.filename().string());
        return 0;
    }

    if ("curves"s == argv[1])           // show supported ECDH curves.
        cout &lt;&lt; ECDH{};

    else if ("init"s == argv[1])        // initiator key construction
    {
                                        // write the file containing
                                        // the curve + public key
        ECDH ecdh{ ECDH::Initiator, "secp384r1", "init.pub" };

                                        // save the initiator's 
                                       // private key
        ecdh.privKey("init.sec", "use your passphrase");
            // not using encryption:
            // auto initSec = Exception::factory&lt;ofstream&gt;("init.sec");
            // initSec &lt;&lt; ecdh.privKey() &lt;&lt; '\n';
    }

    else if ("priv"s == argv[1])        // initiator key construction
    {
                                        // write the file containing
                                        // the curve + public key
        ECDH ecdh{ ECDH::Initiator, "secp384r1", "init.pub" };

        cout &lt;&lt; "wait for the peer's public key. "
                "Press Enter to continue... ";
        cin.ignore(100, '\n');
                                                    // written to file
        auto initShared = Exception::factory&lt;ofstream&gt;("init.shared");
        initShared &lt;&lt; ecdh.sharedKey("peer.pub") &lt;&lt; '\n';  
    }

    else if ("peer"s == argv[1])        // peer's key construction
    {                                   
                                        // write the peer's public key
        ECDH ecdh{ ECDH::Peer, "init.pub", "peer.pub" }; 

                                        // save the peer's private
                                        // key (although not needed)
        auto out = Exception::factory&lt;ofstream&gt;("peer.sec");
        out &lt;&lt; ecdh.privKey() &lt;&lt; '\n';

        out = Exception::factory&lt;ofstream&gt;("peer.shared");
        out &lt;&lt; ecdh.sharedKey() &lt;&lt; '\n';
    }

    else if ("shared"s == argv[1])      // the initiator's shared key
    {                                   // construction
        ECDH ecdh{ "secp384r1", "peer.pub", "init.sec", 
                                            "use your passphrase" };

        auto initShared = Exception::factory&lt;ofstream&gt;("init.shared");
        initShared &lt;&lt; ecdh.sharedKey() &lt;&lt; '\n';     // written to file
    }
    
    else 
    {
        usage(path{ argv[0] }.filename().string());
        return 1;
    }
}
catch (exception const &amp;exc)
{
    cerr &lt;&lt; "Error: " &lt;&lt; exc.what() &lt;&lt; '\n';
    return 1;
}
catch (...)                     // and handle an unexpected exception
{
    cerr &lt;&lt; "unexpected exception\n";
    return 1;
}
</pre>

<p>
<h2 >FILES</h2>
    <em >bobcat/ecdh</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >bigint</strong>(3bobcat), <strong >diffiehellman</strong>(3bobcat),
    <strong >isymcryptstream</strong>(3bobcat), <strong >osymcryptstream</strong>(3bobcat)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
