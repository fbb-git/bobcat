<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>FBB::EoiBuf(3bobcat)</title>
<style type="text/css">
    figure {text-align: center;}
    img {vertical-align: center;}
    .XXfc {margin-left:auto;margin-right:auto;}
    .XXtc {text-align: center;}
    .XXtl {text-align: left;}
    .XXtr {text-align: right;}
    .XXvt {vertical-align: top;}
    .XXvb {vertical-align: bottom;}
</style>
<link rev="made" href="mailto:Frank B. Brokken: f.b.brokken@rug.nl">
</head>
<body text="#27408B" bgcolor="#FFFAF0">
<hr/>
<h1 id="title">FBB::EoiBuf(3bobcat)</h1>
<h2 id="author">End-Of-Information Base class<br/>(libbobcat-dev_6.02.02)</h2>
<h2 id="date">2005-2022</h2>


<p>
<h2 >NAME</h2>FBB::EoiBuf - std::streambuf class offering an eoi manipulator
<p>
<h2 >SYNOPSIS</h2>
    <strong >#include &lt;bobcat/eoibuf&gt;</strong><br/>
    Linking option: <em >-lbobcat</em>
<p>
<h2 >DESCRIPTION</h2>
    The class <em >EoiBuf</em> inherits from <em >Eoi</em> and may therefore be
used as a base class of classes specializing <em >std::streambuf</em>. It also
provides a configurable character buffer for storing characters received from
their devices. Often, when deriving classes from <em >std::streambuf</em> the
derived classes must implement storage to and retrieval from a character
buffer. By deriving from <em >EoiBuf</em> buffer-handling is automatically
provided: it contains a (resizable) character buffer, and offers a <em >setp</em>
member as well as a <em >setg</em> member that can be used to specify the index rage
of the buffer from which extractions receive characters. Also, when overriding
its base class's <em >eoi_</em> member it can be used to signal the end of input
inserted into <em >std::ostream</em> classes using <em >EoiBuf</em> objects as
<em >std::streambuf</em> objects.
<p>
<h2 >NAMESPACE</h2>
    <strong >FBB</strong><br/>
    All constructors, members, operators and manipulators, mentioned in this
man-page, are defined in the namespace <strong >FBB</strong>.
<p>
<h2 >INHERITS FROM</h2>
    <em >FBB::Eoi</em> (and thus from: <em >std::streambuf</em>)
<p>
<h2 >PROTECTED CONSTRUCTOR</h2>
<p>
Analogously to <em >std::streambuf</em> only protected constructors are
        available.
<p>
<ul>
    <li> <strong >EoiBuf()</strong>:<br/>
       The default constructor initializes an empty buffer.
<p>
<li> <strong >EoiBuf(size_t size)</strong>:<br/>
       This initializes an empty buffer of a predefined size of <em >size</em>
        characters .
    </ul>
<p>
Note that there's no inherent limit to the size of the internal buffer: its
    size can always be enlarged or reduced.
<p>
Copy and move constructors (and assignment operators) are not available.
<p>
<h2 >PROTECTED MEMBER FUNCTIONS</h2>
<p>
All members of <em >std:streambuf</em> and <em >Eoi</em> are available, as
    <strong >FBB::EoiBuf</strong> inherits from these classes.
<p>
<ul>
    <li> <strong >std::string &amp;buffer()</strong>:<br/>
       A reference to the internal buffer is returned;
<p>
<li> <strong >size_t bufSize() const</strong>:<br/>
       The length (size) of the internal buffer is returned;
<p>
<li> <strong >void resize(size_t size)</strong>:<br/>
       The size of the internal buffer is changed to <em >size</em> characters;
<p>
<li> <strong >void setg(unsigned next, unsigned beyond)</strong>:<br/>
       The <em >streambuf::eback</em> member returns the address of the internal
        buffer's first character, <em >streambuf::gptr</em> returns the address of
        the internal buffer's <em >next</em> character, <em >streambuf::egptr</em> returns
        the the address of the internal buffer's <em >beyond</em> character;
<p>
<li> <strong >void setp()</strong>:<br/>
       The <em >streambuf::pbase</em> and <em >pptr</em> members return the address of
        the internal buffer's first character, <em >streambuf::epptr</em> returns
        the address immediately beyond the internal buffer's last character;
<p>
<li> <strong >unsigned char *ucharPtr()</strong>:<br/>
       The address of the first character of the internal buffer is returned
        as a pointer to an unsigned character;
<p>
<li> <strong >unsigned char const *ucharPtr() const</strong>:<br/>
       Same as the previous member, but this time the address of the first
        character of the internal buffer is returned as a pointer to an
        immutable unsigned character.
    </ul>
<p>
<h2 >PROTECTED STATIC MEMBER FUNCTIONS</h2>
<p>
The following two static members are provided as convenient functions for
        derived classes to convert the address of the first character of
        <em >std::string</em> objects to pointers to unsigned characters:
<p>
<li> <strong >unsigned char *ucharPtr(std::string &amp;str)</strong>:<br/>
       The address of the first character of <em >str</em> is returned as a pointer
        to an unsigned character;
<p>
<li> <strong >unsigned char const *ucharPtr(std::string const &amp;str) const</strong>:<br/>
       Same as the previous member, but this time the address of the first
        character of <em >str</em> is returned as a pointer to an immutable unsigned
        character.
<p>
<h2 >EXAMPLE</h2>
    The following two functions are defined in the (internally used only)
class <em >FBB::OSymCryptBase</em>, which is the base class of
<strong >osymcryptstreambuf</strong>(3bobcat).
    <ul>
    <li> <em >evpUpdate</em> updates the ongoing encryption or decryption, e.g., using
        the openssl function <em >EVP_EncryptUpdate</em>, using <em >ucharPtr</em> to
        access characters currently in <em >EoiBuf's</em> buffer:
<p>
<pre >
#include "osymcryptbase.ih"

void OSymCryptBase::evpUpdate()
{
    size_t inBufRead = pptr() - pbase();    // # read chars

    checkOutBufSize(inBufRead);

    int nOutputChars;

    if (not ((*d_evpUpdate)(                // en/decrypt the bytes in d_inBuf
            ctx(),
            uOutBuf(), &amp;nOutputChars,
            ucharPtr(), inBufRead
        ))
    )
        throw Exception{} &lt;&lt; "EVP_{En,De}cryptUpdate failed";

    d_outStream.write(outBuf(), nOutputChars);      // write the processed
                                                    // chars to d_outSteam

    setp();
}
</pre>

<p>
<li> <em >eoi_</em> overrides <em >FBB::Eoi::eoi_()</em> function, preventing new
        information from being inserted into the <em >FBB::OSymCryptBase</em> object
        (and thus from being inserted into its <em >FBB::OSymCryptStreambuf</em>
        derived class):
<p>
<pre >
#include "osymcryptbase.ih"

void OSymCryptBase::eoi_()
{
    if (d_eoi)
        return;

    evpUpdate();                // process available chars in the input
    d_eoi = true;
    resize(0);                  // clear the input buffer
    evpUpdate();                // update an empty buffer

}
</pre>

    </ul>
<p>
<h2 >FILES</h2>
    <em >bobcat/eoibuf</em> - defines the class interface
<p>
<h2 >SEE ALSO</h2>
    <strong >bobcat</strong>(7), <strong >eoi</strong>(3bobcat)
<p>
<h2 >BUGS</h2>
    None Reported.
<p>

<h2 >BOBCAT PROJECT FILES</h2>
<p>
<ul>
    <li> <em >https://fbb-git.gitlab.io/bobcat/</em>: gitlab project page;
    <li> <em >bobcat_6.02.02-x.dsc</em>: detached signature;
    <li> <em >bobcat_6.02.02-x.tar.gz</em>: source archive;
    <li> <em >bobcat_6.02.02-x_i386.changes</em>: change log;
    <li> <em >libbobcat1_6.02.02-x_*.deb</em>: debian package containing the
            libraries;
    <li> <em >libbobcat1-dev_6.02.02-x_*.deb</em>: debian package containing the
            libraries, headers and manual pages;
    </ul>
<p>
<h2 >BOBCAT</h2>
    Bobcat is an acronym of `Brokken's Own Base Classes And Templates'.
<p>
<h2 >COPYRIGHT</h2>
    This is free software, distributed under the terms of the
    GNU General Public License (GPL).
<p>
<h2 >AUTHOR</h2>
    Frank B. Brokken (<strong >f.b.brokken@rug.nl</strong>).
<p>
</body>
</html>
