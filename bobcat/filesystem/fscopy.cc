#include "filesystem.ih"

bool FileSystem::fsCopy(Path const &dest, unsigned options) const
{
    fs::copy_options opts = static_cast<fs::copy_options>(options);

    if (d_ec == 0)
        fs::copy(d_path, dest, opts);
    else
        fs::copy(d_path, dest, opts, *d_ec);

    return true;
}
