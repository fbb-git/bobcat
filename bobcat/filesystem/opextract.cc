#include "filesystem.ih"

namespace FBB
{
    istream &operator>>(istream &in, FileSystem &rhs)
    {
        rhs.d_ec = 0;
        return in >> rhs.d_path;
    }

}

