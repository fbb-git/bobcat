#include "filesystem.ih"

FileSystem::FileSystem(Path &&tmp, bool useEC)
:
    d_ec(useEC ? &s_errorCode : 0),
    d_path(move(tmp))
{}
