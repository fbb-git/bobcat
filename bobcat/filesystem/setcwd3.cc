#include "filesystem.ih"

// static
FileSystem FileSystem::setCwd(Path const &path, EC &ec)
{
    FileSystem cwd{ path };
    fs::current_path(path, ec);
    return cwd;
}
