#include <iostream>
#include <string>
#include <exception>

#include <bobcat/filesystem> 

using namespace std;
using namespace FBB;

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        cout << "1st arg: an existing file\n"
                "2nd arg (optional): a directory owned by the caller\n";
        return 1;
    }

    FileSystem fs{ argv[1] };
    if (not fs.exists())
        cout << "No such file " << argv[1] << '\n';
    else
        cout << "last modification date of " << argv[1] << ": " << 
                                            fs.modification() << '\n';

    cout << oct << "Permissions: 0" << 
                    static_cast<size_t>(fs.permissions()) << dec << '\n';

    if (argc > 2)
    {
        cout << "Entries of " << argv[2] << ":\n";
        FileSystem fs2{ argv[2] };
        for (auto const &entry: fs2.dirRange())
            cout << "   " << entry << '\n';
        cout << fs2.errorCode().message() << '\n';
    }
}









