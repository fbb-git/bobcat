#include "filesystem.ih"

bool FileSystem::resize(uintmax_t size) const
{
    if (d_ec == 0)
    {
        fs::resize_file(d_path, size);
        return true;
    }

    fs::resize_file(d_path, size, *d_ec);
    return static_cast<bool>(*d_ec);
}
