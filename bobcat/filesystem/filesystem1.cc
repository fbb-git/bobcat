#include "filesystem.ih"

FileSystem::FileSystem(Path const &path, bool useEC)
:
    d_ec(useEC ? &s_errorCode : 0),
    d_path(path)
{}
