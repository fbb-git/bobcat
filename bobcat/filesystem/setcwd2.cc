#include "filesystem.ih"

// static
FileSystem FileSystem::setCwd(Path const &path)
{
    FileSystem cwd{ path };
    fs::current_path(path);
    return cwd;
}
