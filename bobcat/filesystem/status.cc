#include "filesystem.ih"

FileSystem::FileStatus FileSystem::status(bool destination) const
{
    return destination ?       // follow a symlink 
                optEC1(fs::status, fs::status)
            :
                optEC1(fs::symlink_status, fs::symlink_status);
}
