#include "filesystem.ih"

// static 
FileSystem FileSystem::tmpDir(bool ec)
{
    return ec == true ?
                tmpDir(s_errorCode)
            :
                FileSystem{ std::filesystem::temp_directory_path() };
}

