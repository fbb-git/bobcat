#include "filesystem.ih"

bool FileSystem::optEC3( Path const &dest,
                        void (*fun1)(Path const &, Path const &),
                        void (*fun2)(Path const &, Path const &, EC &)) const
{
    if (d_ec == 0)
    {
        fun1(d_path, dest);
        return true;
    }
    
    (*fun2)(d_path, dest, *d_ec);
    return static_cast<bool>(*d_ec);
}

