#include "filesystem.ih"

bool FileSystem::symLink(Path const &dest) const
{
    return is_directory(d_path, s_errorCode) ?
                optEC3(dest, fs::create_directory_symlink, 
                            fs::create_directory_symlink)
            :
                optEC3(dest, fs::create_symlink, 
                            fs::create_symlink);
}

