inline SteadyClock::SteadyClock(TimePoint const &timePoint)
:
    ClockBase<ChronoClock>(timePoint)
{}

inline SteadyClock &SteadyClock::operator-=(SteadyClock const &rhs)
{
    return *this = SteadyClock{ timePoint() - rhs.elapsed() };
}

inline SteadyClock operator-(SteadyClock const &lhs, SteadyClock const &rhs)
{
    return SteadyClock{ lhs } -= rhs;
}

inline SteadyClock::Duration since(SteadyClock const &time0)
{
    return ClockTypes::elapsed((SteadyClock{} - time0).timePoint());
}

inline size_t countSince(SteadyClock const &time0)
{
    return ClockTypes::count((SteadyClock{} - time0).timePoint());
}
