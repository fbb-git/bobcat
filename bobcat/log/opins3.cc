#include "log.ih"

namespace FBB
{
    Log &operator<<(Log &log, ostream &(*fun)(ostream &))
    {
        if (log.d_active->levelOK or log.d_active->opfunOK)
            static_cast<std::ostream &>(log) << fun;

        if (static_cast<ostream &(*)(ostream &)>(&std::endl) == fun)
            log.d_active->opfunOK = false;
    
        return log;
    }
//         if (log.d_active->opfunOK)
//         {
// //            cerr << __FILE__ " active\n";
// 
//             static_cast<std::ostream &>(log) << fun;
// 
//             if (static_cast<ostream &(*)(ostream &)>(&std::endl) == fun)
//             {
// //                cerr << __FILE__ " active ends\n";
//                 log.d_active->opfunOK = false;
//             }
//         }
//         return log;
//     }
}



