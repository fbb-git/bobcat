#include "log.ih"

void Log::open(string const &filename, ios::openmode mode, char const *delim)
{
    if (filename.empty() || filename == "&1")
        setStream(cout);            // merely set LogBuf's d_stream to &cout
    else if (filename == "&2")
        setStream(cerr);
    else
    {
        d_stream.open(filename.c_str(), mode);
        if (not d_stream)
            throw Exception{} << "Log::Log(string, ...): can't write `" <<
                                                        filename << '\'';
        setStream(d_stream);
    }

    settimestamp(TIMESTAMPS, delim);
    init();
}
