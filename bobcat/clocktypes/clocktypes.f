template <typename Dest, typename Src>
inline auto toClock(Src const &src)
{
    return std::chrono::clock_cast<typename Dest::ChronoClock>(
                                                            src.timePoint());
}

template <typename Dest, typename Src>
inline Dest toDuration(Src const &src)
{
    return std::chrono::duration_cast<Dest>(src);
}

template <typename Dest, typename Src>
inline double toDouble(Src const &src)
{
    return static_cast<double>(src.count()) * 
                            Dest::period::den * Src::period::num /
                            (Dest::period::num * Src::period::den);
}

// static
inline ClockTypes::Period ClockTypes::period()
{
    return Duration::period{};
}

// static
inline ClockTypes::NumType ClockTypes::den()
{
    return Period::den;
}

// static
inline ClockTypes::NumType ClockTypes::num()
{
    return Period::num;
}

// static
inline ClockTypes::Duration ClockTypes::zero()
{
    return Duration::zero();
}

// static 
template <typename TimePoint>
inline ClockTypes::Duration ClockTypes::elapsed(TimePoint const &tp)
{
    return tp.time_since_epoch();
}

// static 
template <typename TimePoint>
inline size_t ClockTypes::count(TimePoint const &tp)
{
    return elapsed(tp).count();
}

